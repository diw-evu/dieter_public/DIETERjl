To run the model you can use this script:
```julia
using DIETER
using HiGHS
using GAMS
using JuMP


datapath = joinpath(pwd(),"testa")
resultspath = joinpath(pwd(),"data_output")

vars = [
    (:G,             [:n, :tech, :h],   :Value,    :Variable),
    (:CU,            [:n, :tech, :h],   :Value,    :Variable),
    (:STO_IN,        [:n, :sto, :h],    :Value,    :Variable),
    (:STO_OUT,       [:n, :sto, :h],    :Value,    :Variable),
    (:STO_L,         [:n, :sto, :h],    :Value,    :Variable),
    (:N_TECH,        [:n, :tech],       :Value,    :Variable),
    (:N_STO_E,       [:n, :sto],        :Value,    :Variable),
    (:N_STO_P_IN,    [:n, :sto],        :Value,    :Variable),
    (:N_STO_P_OUT,   [:n, :sto],        :Value,    :Variable),
    (:EnergyBalance, [:n, :h],          :Marginal, :Constraint),
    (:Load,          [:n, :h],          :Value,    :Parameter),
    (:G_INF,         [:h],              :Value,    :Variable),
    (:Z,             Array{Symbol,1}(), :Value,    :Objective),
]

for scen in 1:2
    # # Comment/uncomment either HIGHS or GAMS
    # # HIGHS solver
    # m = Model(HiGHS.Optimizer)
    # set_optimizer_attribute(m, "solver", "ipm")
    # set_optimizer_attribute(m, "parallel", "on")
    # set_optimizer_attribute(m, "threads", 8)
    # set_optimizer_attribute(m, "highs_debug_level", 3)
    # # GAMS solver
    m = Model(GAMS.Optimizer)
    set_optimizer_attribute(m, GAMS.SysDir(), "/home/cgaete/app/gams/38.2/")
    set_optimizer_attribute(m, GAMS.Solver(), "CPLEX")
    set_optimizer_attribute(m, GAMS.ModelType(), "LP")
    set_optimizer_attribute(m, "lpmethod", 4)
    dtr = DieterModel(datapath)
    define_model!(m, dtr, maxhours=8760, scen=scen)
    optimize!(m)
    collect_results(dtr,m,vars)
    save_scenario(dtr, resultspath, custom_config=Dict{String,Any}())
end

```
