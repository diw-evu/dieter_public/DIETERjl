###############################################################################
## Test run dieter.jl
## 
###############################################################################
# Load packages
using DIETER
using JuMP

# Select solver
# engine = "Gurobi"
engine = "GAMS"

if engine == "GAMS"
    using GAMS
    solver = GAMS.Optimizer
elseif engine == "Gurobi"
    using Gurobi
    solver = Gurobi.Optimizer
else
    using HiGHS
    solver = HiGHS.Optimizer
end


# set paths
datapath = joinpath(pwd(),"data_test_default")
resultspath = joinpath(pwd(),"data_output_default")


# initialize JumP model
m = Model(solver)

if engine == "GAMS"
    set_optimizer_attribute(m, GAMS.SysDir(), ENV["GAMS_DIR"])
    set_optimizer_attribute(m, GAMS.Solver(), "CPLEX")
    set_optimizer_attribute(m, GAMS.ModelType(), "LP")
    set_optimizer_attribute(m, "lpmethod", 4)
end 

# Load data and define model
dtr = DieterModel(datapath, dense_or_sparse=:dense, verbose=true)
define_model!(m, dtr, maxhours=336, scen=1)

# optimize
optimize!(m)

# Check results and determine infeasibility
status = termination_status(m)
if status == MOI.INFEASIBLE && engine == "Gurobi"
    find_infeasible_constraints(m)
else
    println("The model is solved to optimality.")
    println("The objective function value is: $(objective_value(m))")
end


# Select variables to export
exog = [
    (:EnergyBalance, [:n, :h],          :Marginal, :Constraint),
    (:Z,             Array{Symbol,1}(), :Value,    :Objective),
]
pars = collect_symbols(dtr, m, :Parameter) # All parameters
vars = collect_symbols(dtr, m, :Variable)  # All variables

symbs = [exog;pars;vars];

collect_results(dtr,m,symbs)
save_scenario(dtr, m, resultspath, custom_config=Dict{String,Any}("custom_name" => "Scen1"))
