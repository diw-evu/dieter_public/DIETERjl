###############################################################################
## Test run dieter.jl
## 
###############################################################################
# Load packages
using DIETER
using JuMP
using DataFrames
using CSV

# Select solver
# engine = "Gurobi"
engine = "GAMS"

if engine == "GAMS"
    using GAMS
    solver = GAMS.Optimizer
elseif engine == "Gurobi"
    using Gurobi
    solver = Gurobi.Optimizer
else
    using HiGHS
    solver = HiGHS.Optimizer
end


datapath = joinpath(pwd(),"data_test_dataframes")
resultspath = joinpath(pwd(),"data_output_dataframes")


dfdict = Dict{Symbol,DataFrame}()
for (feat,v) in symbols, (symbol, vv) in v
    if symbol == :con
        dfdict[symbol] = CSV.read(joinpath(datapath,"cons.csv"), DataFrame)
    else
        dfdict[symbol] = CSV.read(joinpath(datapath,"$symbol.csv"), DataFrame)
    end
end


# initialize JumP model
m = Model(solver)

if engine == "GAMS"
    set_optimizer_attribute(m, GAMS.SysDir(), ENV["GAMS_DIR"])
    set_optimizer_attribute(m, GAMS.Solver(), "CPLEX")
    set_optimizer_attribute(m, GAMS.ModelType(), "LP")
    set_optimizer_attribute(m, "lpmethod", 4)
end 

# Load data and define model
dtr = DieterModel(dfdict, dense_or_sparse=:sparse, verbose=true)
define_model!(m, dtr, maxhours=336, scen=1)

# optimize
optimize!(m)

# Check results and determine infeasibility
status = termination_status(m)
if status == MOI.INFEASIBLE && engine == "Gurobi"
    find_infeasible_constraints(m)
else
    println("The model is solved to optimality.")
    println("The objective function value is: $(objective_value(m))")
end

# Select variables to export
exog = [
    (:EnergyBalance, [:n, :h],          :Marginal, :Constraint),
    (:Z,             Array{Symbol,1}(), :Value,    :Objective),
]
pars = collect_symbols(dtr, m, :Parameter) # All parameters
vars = collect_symbols(dtr, m, :Variable)  # All variables

symbs = [exog;pars;vars];

collect_results(dtr,m,symbs)
save_scenario(dtr, m, resultspath, custom_config=Dict{String,Any}("custom_name" => "Scen1"))
