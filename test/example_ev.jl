###############################################################################
## Test EV module
## 
###############################################################################

using DIETER
using JuMP
using DataFrames

engine = "GAMS"
datapath = joinpath(pwd(),"data_test_01")
resultspath = joinpath(pwd(),"data_output_ev_multi")
maxhours = 336
nr_scenarios = 10


if engine == "GAMS" 
    using GAMS
    solver = GAMS.Optimizer
elseif engine == "HiGHS"
    using HiGHS
    solver = HiGHS.Optimizer
end

# Activate all extension to load all data. Then, during the iteration some extension can be deactivated accordingly.
df = DataFrame(n=["DE"], ev=[1], heat=[0])

# Load data and define model
dieter_data = DieterModel(datapath; dataframe_dict=Dict(:ext => df));

##########################
### Here scenario runs ###
##########################



L = dieter_data.sets[:l]
TECH = dieter_data.sets[:tech]
STO = dieter_data.sets[:sto]
N = dieter_data.sets[:n]
H = dieter_data.sets[:h]
EV = dieter_data.sets[:ev];

# Activation of extension dict

extensions = Dict{Int,Dict{Symbol,Any}}()
extensions[1] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[2] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[2][:ev]["DE"] = 1
extensions[3] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[3][:ev]["DE"] = 1
extensions[4] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[4][:ev]["DE"] = 1
extensions[5] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[5][:ev]["DE"] = 1
extensions[6] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[7] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[7][:ev]["DE"] = 1
extensions[8] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[8][:ev]["DE"] = 1
extensions[9] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[9][:ev]["DE"] = 1
extensions[10] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[10][:ev]["DE"] = 1

# Modify or replace parameters

max_flow            = Dict(l => 0 for l in L)
ev_catenary         = Dict((n,ev)=> 0 for n in ["DE"], ev in EV)
ev_discharge        = Dict((n,ev)=> 0 for n in ["DE"], ev in EV)
ev_exog             = Dict((n,ev)=> 0 for n in ["DE"], ev in EV)

ev_catenary_active  = Dict((n,ev)=> 1 for n in ["DE"], ev in EV)
ev_discharge_active = Dict((n,ev)=> 1 for n in ["DE"], ev in EV)
ev_exog_active      = Dict((n,ev)=> 1 for n in ["DE"], ev in EV);

ev_quant     = Dict(n=> 1000000 for n in ["DE"])

ev_exog_use_ed = Dict((n,ev)=> 1 for n in ["DE"], ev in EV)

ev_min_soc     = Dict((n,ev)=> 0.2 for n in ["DE"], ev in EV)


# Scenarios dict

scenarios    = Dict{Int,Dict{Symbol,Any}}()

scenarios[1] = Dict(
    :max_flow       => max_flow,
                    )
scenarios[2] = Dict(
    :max_flow       => max_flow,
    :ev_catenary    => ev_catenary,
    :ev_discharge   => ev_discharge,
    :ev_exog        => ev_exog,
    :ev_quant       => ev_quant,
    :ev_exog_use_ed => ev_exog_use_ed,
    :ev_min_soc     => ev_min_soc
                    )
scenarios[3] = Dict(
    :max_flow       => max_flow,
    :ev_catenary    => ev_catenary_active,
    :ev_discharge   => ev_discharge,
    :ev_exog        => ev_exog,
    :ev_quant       => ev_quant,
    :ev_exog_use_ed => ev_exog_use_ed,
    :ev_min_soc     => ev_min_soc
                    )
scenarios[4] = Dict(
    :max_flow       => max_flow,
    :ev_catenary    => ev_catenary,
    :ev_discharge   => ev_discharge_active,
    :ev_exog        => ev_exog,
    :ev_quant       => ev_quant,
    :ev_exog_use_ed => ev_exog_use_ed,
    :ev_min_soc     => ev_min_soc
                    )
scenarios[5] = Dict(
    :max_flow       => max_flow,
    :ev_catenary    => ev_catenary,
    :ev_discharge   => ev_discharge,
    :ev_exog        => ev_exog_active,
    :ev_quant       => ev_quant,
    :ev_exog_use_ed => ev_exog_use_ed,
    :ev_min_soc     => ev_min_soc
                    )
scenarios[6] = Dict(
    # :max_flow       => max_flow,
                    )
scenarios[7] = Dict(
    # :max_flow       => max_flow,
    :ev_catenary    => ev_catenary,
    :ev_discharge   => ev_discharge,
    :ev_exog        => ev_exog,
    :ev_quant       => ev_quant,
    :ev_exog_use_ed => ev_exog_use_ed,
    :ev_min_soc     => ev_min_soc
                    )
scenarios[8] = Dict(
    # :max_flow       => max_flow,
    :ev_catenary    => ev_catenary_active,
    :ev_discharge   => ev_discharge,
    :ev_exog        => ev_exog,
    :ev_quant       => ev_quant,
    :ev_exog_use_ed => ev_exog_use_ed,
    :ev_min_soc     => ev_min_soc
                    )
scenarios[9] = Dict(
    # :max_flow       => max_flow,
    :ev_catenary    => ev_catenary,
    :ev_discharge   => ev_discharge_active,
    :ev_exog        => ev_exog,
    :ev_quant       => ev_quant,
    :ev_exog_use_ed => ev_exog_use_ed,
    :ev_min_soc     => ev_min_soc
                    )
scenarios[10] = Dict(
    # :max_flow       => max_flow,
    :ev_catenary    => ev_catenary,
    :ev_discharge   => ev_discharge,
    :ev_exog        => ev_exog_active,
    :ev_quant       => ev_quant,
    :ev_exog_use_ed => ev_exog_use_ed,
    :ev_min_soc     => ev_min_soc
                    );

#  Add custom names to metadata while storing results

custom_name = Dict{Int,String}()
custom_name[1] = "No EVs"
custom_name[2] = "Smart"
custom_name[3] = "Smart+catenary"
custom_name[4] = "V2G"
custom_name[5] = "Inflexible"
custom_name[6] = "No EVs"
custom_name[7] = "Smart"
custom_name[8] = "Smart+catenary"
custom_name[9] = "V2G"
custom_name[10]= "Inflexible"

custom_flow = Dict{Int,String}()
custom_flow[1] = "No Flow"
custom_flow[2] = "No Flow"
custom_flow[3] = "No Flow"
custom_flow[4] = "No Flow"
custom_flow[5] = "No Flow"
custom_flow[6] = "Flow"
custom_flow[7] = "Flow"
custom_flow[8] = "Flow"
custom_flow[9] = "Flow"
custom_flow[10]= "Flow"

@assert length(extensions) == nr_scenarios "length of extensions dict must be equal than nr_scenarios"
@assert length(scenarios) == nr_scenarios "length of scenarios dict must be equal than nr_scenarios"
@assert length(custom_name) == nr_scenarios "length of custom_name dict must be equal than nr_scenarios"

# Runs
for i in 1:nr_scenarios
    dtr = deepcopy(dieter_data)
    dtr.ext = extensions[i]

    metadata = Dict{Symbol,Any}()
    for (par,data) in scenarios[i]
        for (coords,value) in data
            dtr.parameters[par][coords] = value
        end
    end
    metadata[:custom_name] = custom_name[i]
    metadata[:custom_flow] = custom_flow[i]

    # Template below
    m = Model(solver)

    if engine == "GAMS"
        set_optimizer_attribute(m, GAMS.SysDir(), ENV["GAMS_DIR"])
        set_optimizer_attribute(m, GAMS.Solver(), "CPLEX")
        set_optimizer_attribute(m, GAMS.ModelType(), "LP")
        set_optimizer_attribute(m, "lpmethod", 4)
    elseif engine == "HiGHS"
        set_optimizer_attribute(m, "solver", "ipm")
        set_optimizer_attribute(m, "parallel", "on")
        set_optimizer_attribute(m, "threads", 8)
        set_optimizer_attribute(m, "highs_debug_level", 3)
    end 

    define_model!(m, dtr, maxhours=maxhours, scen=i)

    # optimize
    optimize!(m)

    println("Termination status is: $(termination_status(m))")
    println("The objective function value is: $(objective_value(m))")

    # Select variables and parameters to export
    exog = [
        (:EnergyBalance, [:n, :h],          :Marginal, :Constraint),
        (:Z,             Array{Symbol,1}(), :Value,    :Objective),
    ]
    pars = collect_symbols(dtr, m, :Parameter) # All parameters
    vars = collect_symbols(dtr, m, :Variable)  # All variables

    symbs = [exog;pars;vars];

    collect_results(dtr,m,symbs)
    save_scenario(dtr, m, resultspath, custom_config= metadata)
end