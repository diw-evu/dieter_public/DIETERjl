###############################################################################
## Test EV module
## 
###############################################################################

using DIETER
using JuMP
using DataFrames

engine = "GAMS"
datapath = joinpath(pwd(),"data_test_01")
resultspath = joinpath(pwd(),"data_output_ev_exog")
maxhours = 336
nr_scenarios = 4


if engine == "GAMS" 
    using GAMS
    solver = GAMS.Optimizer
elseif engine == "HiGHS"
    using HiGHS
    solver = HiGHS.Optimizer
end

# Activate all extension to load all data. Then, during the iteration some extension can be deactivated accordingly.
df = DataFrame(n=["DE"], ev=[1], heat=[0])

# Load data and define model
dieter_data = DieterModel(datapath; dataframe_dict=Dict(:ext => df));

##########################
### Here scenario runs ###
##########################

L = dieter_data.sets[:l]
TECH = dieter_data.sets[:tech]
STO = dieter_data.sets[:sto]
N = dieter_data.sets[:n]
H = dieter_data.sets[:h]
EV = dieter_data.sets[:ev];

# Activation of extension dict

extensions = Dict{Int,Dict{Symbol,Any}}()
extensions[1] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[2] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[2][:ev]["DE"] = 1
extensions[3] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[3][:ev]["DE"] = 1
extensions[4] = Dict(ext => Dict(n => 0 for n in N) for ext in dieter_data.options)
extensions[4][:ev]["DE"] = 1

# Modify or replace parameters

# Set to zero
max_flow     = Dict(l => -1 for l in L)
ev_catenary  = Dict((n,ev)=> 0 for n in ["DE"], ev in EV)
ev_discharge = Dict((n,ev)=> 0 for n in ["DE"], ev in EV)
ev_exog      = Dict((n,ev)=> 0 for n in ["DE"], ev in EV)
phi_min_res  = Dict(n=> 0.0 for n in N)
ev_quant     = Dict(n=> 1000000 for n in N)
ev_share     = Dict((n,ev)=> 0.0 for n in N, ev in EV)

#  edit some Nodes
phi_min_res_de80 = deepcopy(phi_min_res)
phi_min_res_de80["DE"] = 0.8

ev_exog_scen2 = deepcopy(ev_exog)
ev_exog_scen2["DE","ev01"] = 0
ev_exog_scen2["DE","ev02"] = 0
ev_exog_scen2["DE","ev03"] = 0
ev_exog_scen2["DE","ev04"] = 0

ev_exog_scen3 = deepcopy(ev_exog)
ev_exog_scen3["DE","ev01"] = 1
ev_exog_scen3["DE","ev02"] = 1
ev_exog_scen3["DE","ev03"] = 1
ev_exog_scen3["DE","ev04"] = 1

ev_exog_scen4 = deepcopy(ev_exog)
ev_exog_scen4["DE","ev01"] = 0
ev_exog_scen4["DE","ev02"] = 1
ev_exog_scen4["DE","ev03"] = 0
ev_exog_scen4["DE","ev04"] = 1

ev_share["DE","ev01"] = 0.25
ev_share["DE","ev02"] = 0.25
ev_share["DE","ev03"] = 0.25
ev_share["DE","ev04"] = 0.25


# Scenarios dict

scenarios    = Dict{Int,Dict{Symbol,Any}}()

scenarios[1] = Dict(
    :max_flow       => max_flow,
                    )
scenarios[2] = Dict(
    :max_flow       => max_flow,
    :ev_catenary    => ev_catenary,
    :ev_discharge   => ev_discharge,
    :ev_exog        => ev_exog_scen2,
    :phi_min_res    => phi_min_res_de80,
    :ev_quant       => ev_quant,
    :ev_share       => ev_share
                    )
scenarios[3] = Dict(
    :max_flow       => max_flow,
    :ev_catenary    => ev_catenary,
    :ev_discharge   => ev_discharge,
    :ev_exog        => ev_exog_scen3,
    :phi_min_res    => phi_min_res_de80,
    :ev_quant       => ev_quant,
    :ev_share       => ev_share
                    )
scenarios[4] = Dict(
    :max_flow       => max_flow,
    :ev_catenary    => ev_catenary,
    :ev_discharge   => ev_discharge,
    :ev_exog        => ev_exog_scen4,
    :phi_min_res    => phi_min_res_de80,
    :ev_quant       => ev_quant,
    :ev_share       => ev_share
                    )


#  Add custom names to metadata while storing results

custom_name = Dict{Int,String}()
custom_name[1] = "No EVs"
custom_name[2] = "Smart"
custom_name[3] = "Exog"
custom_name[4] = "Mixed"


custom_flow = Dict{Int,String}()
custom_flow[1] = "Inf Flow"
custom_flow[2] = "Inf Flow"
custom_flow[3] = "Inf Flow"
custom_flow[4] = "Inf Flow"

custom_res = Dict{Int,String}()
custom_res[1] = "0%"
custom_res[2] = "0%"
custom_res[3] = "0%"
custom_res[4] = "0%"

@assert length(extensions) == nr_scenarios "length of extensions dict must be equal than nr_scenarios"
@assert length(scenarios) == nr_scenarios "length of scenarios dict must be equal than nr_scenarios"
@assert length(custom_name) == nr_scenarios "length of custom_name dict must be equal than nr_scenarios"

# Runs
for i in 1:nr_scenarios
    dtr = deepcopy(dieter_data)
    dtr.ext = extensions[i]

    metadata = Dict{Symbol,Any}()
    for (par,data) in scenarios[i]
        for (coords,value) in data
            dtr.parameters[par][coords] = value
        end
    end
    metadata[:custom_name] = custom_name[i]
    metadata[:custom_flow] = custom_flow[i]
    metadata[:custom_res]  = custom_res[i]

    # Template below
    m = Model(solver)

    if engine == "GAMS"
        set_optimizer_attribute(m, GAMS.SysDir(), ENV["GAMS_DIR"])
        set_optimizer_attribute(m, GAMS.Solver(), "CPLEX")
        set_optimizer_attribute(m, GAMS.ModelType(), "LP")
        set_optimizer_attribute(m, "lpmethod", 4)
    elseif engine == "HiGHS"
        set_optimizer_attribute(m, "solver", "ipm")
        set_optimizer_attribute(m, "parallel", "on")
        set_optimizer_attribute(m, "threads", 8)
        set_optimizer_attribute(m, "highs_debug_level", 3)
    end 

    define_model!(m, dtr, maxhours=maxhours, scen=i)

    # optimize
    optimize!(m)

    println("Termination status is: $(termination_status(m))")
    println("The objective function value is: $(objective_value(m))")

    # Select variables and parameters to export
    exog = [
        (:EnergyBalance, [:n, :h],          :Marginal, :Constraint),
        (:Z,             Array{Symbol,1}(), :Value,    :Objective),
    ]
    pars = collect_symbols(dtr, m, :Parameter) # All parameters
    vars = collect_symbols(dtr, m, :Variable)  # All variables

    symbs = [exog;pars;vars];

    collect_results(dtr,m,symbs)
    save_scenario(dtr, m, resultspath, custom_config=metadata)
end