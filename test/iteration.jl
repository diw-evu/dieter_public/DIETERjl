
using Distributed
addprocs(2)

using DataFrames
using CSV
using GAMS
using JuMP
@everywhere using DIETER


input_data_path =           joinpath(pwd(),"data_test_dataframes")
iteration_table_file =      joinpath(pwd(),"iteration","IterationTable.csv")
iteration_timeseries_path = joinpath(pwd(),"iteration")
result_data_path =          joinpath(pwd(),"data_output_iter")

dfdict = Dict{Symbol,DataFrame}()
for (feat,v) in symbols, (symbol, vv) in v
    if symbol in derived_parameters
        continue
    end
    if symbol == :con
        dfdict[symbol] = CSV.read(joinpath(input_data_path,"cons.csv"), DataFrame)
    else
        dfdict[symbol] = CSV.read(joinpath(input_data_path,"$symbol.csv"), DataFrame)
    end
end


println("Loading input data...")
@time dieter_data = DieterModel(dfdict, dense_or_sparse=:sparse, verbose=true)

println("\nReading iteration table...")
@time scenarios = iteration_table(iteration_table_file, dieter_data, iteration_timeseries_path)

println("\nBuilding scenarios container...")
@time begin
    collection = collect_scenarios(dieter_data, GAMS.Optimizer, scenarios, 336, result_data_path, 1, 2)

    for (_,m,_,_,_,_,_,_) in collection
        set_optimizer_attribute(m, GAMS.SysDir(), ENV["GAMS_DIR"])
        set_optimizer_attribute(m, GAMS.Solver(), "CPLEX")
        set_optimizer_attribute(m, GAMS.ModelType(), "LP")
        set_optimizer_attribute(m, "lpmethod", 4)
        set_optimizer_attribute(m, GAMS.Threads(), 1)
    end
end

println("\nStarting optimization...")
@time container = pmap((args)->scenario_optimization(args...), collection)

println("\nSummary...\n")
for msg in sort(container)
    println(msg)
end

println("\n\nOptimization process finished!")
