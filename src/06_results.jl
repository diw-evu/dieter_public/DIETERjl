# Collect results

function dim_elems_to_array_dict(dtr::DieterModel; dims::Vector{Symbol})
    maps = Dict{Symbol, Vector{String}}()
    for dim in dims
        maps[dim] = dtr.sets[dim]
    end
    return maps
end


"""
vars{Vector{Tuple{Symbol, Vector{Symbol}, Symbol, Symbol}}} e.g. [(:G,[:n,:gen,:h],:Value,:Variable),(:EnergyBalance,[:n,:h],:Marginal,:Constraint)]
Warn 'Only constraints can provide Marginal with this function'
value_type: :Value, :Marginal
symbol_type: :Variable, :Constraint, :Parameter, :Objective
"""
function collect_results(dtr::DieterModel, m::JuMP.AbstractModel, symbol_collection::Vector{Tuple{Symbol, Vector{Symbol}, Symbol, Symbol}}, sparse::Bool=false, verbose::Bool=false)
    
    model_name = dtr.settings[:name_scenario]

    if termination_status(m) == OPTIMAL
        model_status = "Optimal"
        if verbose
            println("$model_name: Solution is optimal")
        end
    elseif termination_status(m) == TIME_LIMIT && has_values(m)
        model_status = "Suboptimal"
        if verbose
            println("$model_name: Solution is suboptimal due to a time limit, but a primal solution is available")
        end
    else
        model_status = "Infeasible"
        if verbose
            println("$model_name: The model output is INFEASIBLE.")
        end
    end
    dtr.settings[:model_status] = model_status

    Types = Dict{Symbol,String}(:Marginal => "m", :Value => "v")
    for (symbol_name, dims, value_type, symbol_type) in symbol_collection
        if verbose
            println("Collecting $symbol_name")
        end
        if value_type == :Value
            if symbol_type in [:Constraint,:Variable]
                rawdf = convert_jump_container_to_df(value.(m[symbol_name]), dim_names=dims)
            elseif symbol_type == :Parameter
                if !haskey(dtr.parameters, symbol_name)
                    throw(ArgumentError("$(symbol_name) not found in parameters"))
                end
                if symbol_name in [:extensions]
                    extension_dict = Dict{Any, Any}()
                    for (ext, node_val_dict) in dtr.ext
                        for (node, val) in node_val_dict
                            extension_dict[(node, ext,)] = val
                        end
                    end
                    rawdf = convert_dict_to_df(extension_dict, dim_names=dims, value_name=:value)
                else
                    rawdf = convert_jump_container_to_df(dtr.parameters[symbol_name], dim_names=dims, value_name=:value)
                end
            elseif symbol_type == :Objective
                var = Dict((Nothing,) => objective_value(m))
                value_col = (:value,)
                len_dim = length(dims)
                tup_dim = (dims..., value_col...)
                rawdf = [NamedTuple{tup_dim}(([k[i] for i in 1:len_dim]..., v)) for (k,v) in var] |> DataFrame
            end
            if sparse
                df = rawdf[findall(!isequal(0.0), rawdf.value), :]
            else
                df = rawdf
            end
        elseif value_type == :Marginal
            if symbol_type == :Constraint
                rawdf = convert_jump_container_to_df(dual.(m[symbol_name]), dim_names=dims)
            else
                throw(ArgumentError("$symbol_name: Only a constraint can provide Marginal with this function"))
            end
            if sparse
                df = rawdf[findall(!isequal(0.0), rawdf.value), :]
            else
                df = rawdf
            end
        else
            throw(ArgumentError(":Marginal or :Value expected"))
        end
        df = dropmissing(df, :value)
        if isempty(df)
            if verbose
                println(symbol_name," has no data. Not saved")
            end
            continue
        end
        collection = Dict{Symbol,Any}()
        collection[:coords] = dim_elems_to_array_dict(dtr, dims=dims)
        for (key,values) in collection[:coords]
            df = df[in(values).(df[!,key]), :]
        end
        if isempty(df)
            if verbose
                println(symbol_name," has no data. Not saved")
            end
            continue
        end
        collection[:df] = df
        collection[:value_type] = Types[value_type]
        dtr.results[symbol_name] = collection
    end
    return nothing
end

function save_scenario(dtr::DieterModel, m::JuMP.AbstractModel, resultspath::AbstractString; custom_config::Union{Dict{String,Any}, Dict{Symbol,Any}}=Dict{String,Any}(), compress::Union{Nothing,Symbol}=nothing, save_model::Bool=true)
    # compress: :lz4, :zstd
    scenario_name = dtr.settings[:name_scenario]
    scen_folder = joinpath(resultspath, scenario_name)
    mkpath(scen_folder)
    config_data = Dict{String,Any}()
    config_data["name"] = scenario_name
    config_data["metadata"] = Dict{String,Any}()
    config_data["metadata"]["long_id"] = scenario_name
    config_data["metadata"]["run"] = dtr.settings[:scen_int]
    config_data["metadata"]["status"] = dtr.settings[:model_status]
    config_data["metadata"]["nthhour"] = dtr.settings[:nthhour]
    config_data["metadata"]["maxhours"] = dtr.settings[:maxhours]
    for (key,val) in dtr.scale_dict
        config_data["metadata"]["scale_$key"] = val
    end

    for (key,val) in custom_config
        if isa(key, Symbol)
            key = String(key)
        end
        if haskey(config_data["metadata"], key)
            throw(ArgumentError("key $key from 'custom_config' already exists in metadata"))
        else
            config_data["metadata"][key] = val
        end
    end
    config_file_name = string(scenario_name,".json")
    config_file_path = joinpath(scen_folder,config_file_name)

    model_file_path = joinpath(scen_folder,"model.lp")

    # Save model
    if save_model
        write_to_file(m, model_file_path)
    end

    stringdata = JSON.json(config_data, 4)
    open(config_file_path, "w") do f
            write(f, stringdata)
        end

    for (sym, v) in dtr.results
        symbol_info = Dict{String,Any}()
        symbol_base_file_name = join([String(sym), v[:value_type]],'.')
        symbol_info["symbol_name"] = String(sym)
        symbol_info["value_type"] = v[:value_type]
        symbol_info["scenario_name"] = scenario_name
        symbol_data_file = joinpath(scen_folder, string(symbol_base_file_name,".feather"))

        metadata = Dict{String, String}()
        
        metadata_tool_key = "dieter.jl"
        metadata[metadata_tool_key] = JSON.json(symbol_info)
        metadata_array_key = "karray"
        metadata[metadata_array_key] = JSON.json(Dict{String,Any}("coords" => v[:coords]))

        io = IOBuffer()
        Arrow.write(io, v[:df], metadata=metadata, compress=compress)
        seekstart(io)
        tbl = Arrow.Table(io)
        Arrow.write(symbol_data_file, tbl)
    end
end

function collect_symbols(dtr::DieterModel, m::JuMP.AbstractModel, symbol_type::Symbol=:Parameter, value_type::Symbol=:Value)

    if symbol_type == :Parameter
        parameters_repo::Vector{Tuple{Symbol, Vector{Symbol}, Symbol, Symbol}} = []
        for (param, _) in dtr.parameters
            for (_, dc) in dtr.symbols
                if haskey(dc, param)
                    push!(parameters_repo, (param, dc[param][:dims], value_type, :Parameter,))
                    break
                end
            end
        end
        return parameters_repo

    elseif symbol_type == :Variable
        variable_repo::Vector{Tuple{Symbol, Vector{Symbol}, Symbol, Symbol}} = []
        # Main task: Discover the sets names for each variable
        # First, get the first set element of the variable to later guess where each element belongs to which set.
        symbols_dict = Dict{Symbol,Any}()
        symb_dict = get_variables(m)
        for (key, contr) in symb_dict
            var = value.(contr)
            if var isa JuMP.Containers.DenseAxisArray
                firsttuple = first(zip(var.axes...))
            elseif var isa JuMP.Containers.SparseAxisArray
                firsttuple = first(keys(var.data))
            else
                throw(ArgumentError("JuMP.Containers.DenseAxisArray or JuMP.Containers.SparseAxisArray expected"))
            end
            symbols_dict[key] = firsttuple
        end
        # Second, see in which set each set element belongs
        # it might be the case that an element belongs to multiple sets, we have to discover what is the set and avoid subsets
        variable_repo_tmp::Vector{Tuple{Symbol, Vector{Symbol}, Symbol, Symbol}} = []
        for (var, item) in symbols_dict
            arr_dims = Vector{Symbol}()
            if item isa Tuple
                for elem in item
                    for (dim, coords) in dtr.sets
                        if elem in coords
                            push!(arr_dims, dim)
                            break
                        end
                    end
                end
            else
                for (dim, coords) in dtr.sets
                    if item in coords
                        for (subs, set) in ssets
                            if dim == subs
                                dim = set
                                break
                            end
                        end
                        push!(arr_dims, dim)
                        break
                    end
                end
            end
            push!(variable_repo_tmp, (var, arr_dims, value_type, symbol_type))
        end
        for (name,sets,value_type,category) in variable_repo_tmp
            vec_dims = []
            for set in sets
                if haskey(ssets, set)
                    push!(vec_dims, ssets[set])
                else
                    push!(vec_dims, set)
                end
            end
            push!(variable_repo, (name, vec_dims, value_type, category))
        end
        return variable_repo
    else
        throw(ArgumentError("symbol_type can be either :Parameter or :Variable"))
    end
end