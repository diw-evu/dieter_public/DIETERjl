function isstring(container)
    ret = false
    if eltype(container) === String
        ret = true
    else
        for T in subtypes(InlineString)
            if eltype(container) === T
                ret = true
                break
            end
        end
    end
    return ret
end

function sametype(var::JuMP.Containers.SparseAxisArray, loc::Int, self)
    T = typeof(first(keys(var.data))[loc])
    ST = typeof(self)
    if T === ST
        return self
    elseif T !== ST
        for option in subtypes(InlineString)
            if option === T
                # println("Changed from $ST to $T")
                dc = Dict{T,Any}()     # This is the nasty thing I have ever written :(
                dc[self] = nothing     # Creating a dictionary
                self = first(keys(dc)) # Just to get the same desired DataType. It must be a better way!
                break
            end
        end
        return self
    else
        # println("No change done")
        return self
    end
end

function sametype(var::JuMP.Containers.DenseAxisArray, loc::Int, self)
    T = typeof(var.axes[loc])
    ST = typeof(self)
    if T === ST
        return self
    elseif T !== ST
        for option in subtypes(InlineString)
            if option === T
                # println("Changed from $ST to $T")
                dc = Dict{T,Any}()     # This is the nasty thing I have ever written :(
                dc[self] = nothing     # Creating a dictionary
                self = first(keys(dc)) # Just to get the same desired DataType. It must be a better way!
                break
            end
        end
        return self
    else
        # println("No change done")
        return self
    end
end

function convert_long_df_to_dict(df::DataFrame; value_name::Symbol=:value, allow_inlinestring::Bool=false)
    setsdf = select(df, Not([value_name]))
    if allow_inlinestring
        ids = zip([col for col in eachcol(setsdf)]...) |> collect
    else
        ids = zip([isstring(col) ? string.(col) : col for col in eachcol(setsdf)]...) |> collect
    end
    ndims = length(first(ids))
    return Dict{NTuple{ndims,Any}, Any}(collect(zip(ids, df[!, value_name])))
end

get_variable_names(m::JuMP.Model) = [v.name for (k,v) in m.varData]

function convert_jump_container_to_df(var::JuMP.Containers.DenseAxisArray; dim_names::Vector{Symbol}=Vector{Symbol}(), value_name::Symbol=:value)

    if isempty(dim_names)
        dim_names = [Symbol("x$i") for i in 1:length(var.axes)]
    end
    @assert length(dim_names) == length(var.axes) "Length of given name list does not fit the number of variable dimensions"

    ind = [k for k in Base.Iterators.product(var.axes...)][:]
    vecvec = Vector{Vector{String}}()
    for _ in eachindex(dim_names)
        push!(vecvec, Vector{String}())
    end
    for i in eachindex(ind)
        for n in eachindex(dim_names)
            push!(vecvec[n], ind[i][n])
        end
    end
    list = [(var[(ind[i]...,)...]) for i in eachindex(ind)]
    df = DataFrame(hcat(vecvec...,list), [dim_names...,value_name])

    return df
end

function convert_jump_container_to_df(var::JuMP.Containers.SparseAxisArray; dim_names::Vector{Symbol}=Vector{Symbol}(), value_name::Symbol=:value)
    firstkey = first(keys(var.data))
    ndims = length(firstkey)
    if isempty(dim_names)
        dim_names = [Symbol("x$i") for i in 1:ndims]
    end
    @assert length(dim_names) == ndims "Length of given name list does not fit the number of variable dimensions"
    value_col = (value_name,)
    tup_dim = (dim_names..., value_col...)
    df = [NamedTuple{tup_dim}(([k[i] for i in 1:ndims]..., v)) for (k,v) in var.data] |> DataFrame
    return df
end

function convert_jump_container_to_df(var::Union{Float16,Float32,Float64,Int16,Int32,Int64}; dim_names::Vector{Symbol}=Vector{Symbol}(), value_name::Symbol=:value)
    value_col = (value_name,)
    var_dc = Dict((Nothing,) => var)
    len_dim = length(dim_names)
    tup_dim = (dim_names..., value_col...)
    df = [NamedTuple{tup_dim}(([k[i] for i in 1:len_dim]..., v)) for (k,v) in var_dc] |> DataFrame
    return df
end

function convert_dict_to_df(var::Dict{Any, Any}; dim_names::Vector{Symbol}=Vector{Symbol}(), value_name::Symbol=:value)

    @assert !isempty(dim_names) "Scalar variables are not supported"
    value_col = (value_name,)
    len_dim = length(dim_names)
    tup_dim = (dim_names..., value_col...)
    if len_dim == 1
        df = [NamedTuple{tup_dim}(([k for i in 1:len_dim]..., v)) for (k,v) in var] |> DataFrame
    else
        df = [NamedTuple{tup_dim}(([k[i] for i in 1:len_dim]..., v)) for (k,v) in var] |> DataFrame
    end

    return df
end

# Collect results
function get_variables(model::JuMP.AbstractModel)
    var = JuMP.all_variables(model);
    tmp = []
    for name in name.(var)
        if occursin("[", name)
            key = split.(name, "[")[1]
        else
            key = name
        end
        push!(tmp, key)
    end
    variable_names = unique(tmp)
    variable_names_dict = Dict{Symbol,Any}()
    for var_name in sort!(variable_names)
        variable_names_dict[Symbol(var_name)] = model[Symbol(var_name)]
    end
    return variable_names_dict
end

function sameas(set_1::Vector{String}, set_2::Vector{String})
    return set_1[findall(in(set_2),set_1)]
end

function find_infeasible_constraints(m::JuMP.AbstractModel)
    compute_conflict!(m)
    list_of_conflicting_constraints = ConstraintRef[]
    for (F, S) in list_of_constraint_types(m)
        for con in all_constraints(m, F, S)
            if MOI.get.(m, MOI.ConstraintConflictStatus(),con) == MOI.IN_CONFLICT
                push!(list_of_conflicting_constraints, con)
            end
        end
    end
    print("The model is infeasible because of these conflicting constraints:\n")
    return list_of_conflicting_constraints
end

function reorder(row_set_names::Vector{Symbol}, col_set_name::Symbol, insert_col_set::Int)

    @assert insert_col_set >= -1
    @assert insert_col_set != 0
    @assert insert_col_set <= length(row_set_names) + 1
    order = [set for set in row_set_names]
    if insert_col_set == -1
        push!(order, col_set_name)
    else
        insert!(order, insert_col_set, col_set_name)
    end
    return order
end

function df_stack(data::DataFrame; row_set_names::Vector{Symbol}, col_set_name::Symbol, insert_col_set::Int=-1, value_name::Symbol=:value, skip=Symbol[])

    original_colnames = Symbol.(names(data))
    @assert all(set_name in original_colnames for set_name in row_set_names)
    desired_colnames = [item for item in original_colnames if !(item in skip)]
    col_set_items    = [item for item in desired_colnames if !(item in row_set_names)]
    col_set_length   = length(col_set_items)

    df_col_set = data[!, col_set_items]
    df_row_set = data[!, row_set_names]

    df_col = stack(df_col_set, col_set_items)
    df_col = rename(df_col, :variable => col_set_name)
    df_row = repeat(df_row_set, col_set_length)

    df = hcat(df_row, df_col)
    
    order = reorder(row_set_names, col_set_name, insert_col_set)
    push!(order, value_name)
    return df[!, order]
end

function convert_jump_container_to_dict(var::JuMP.Containers.DenseAxisArray; keepmissing::Bool=true)
    ndims = length(var.axes)
    ind = [k for k in Base.Iterators.product(var.axes...)]
    if keepmissing
        return Dict{NTuple{ndims,Any},Any}([(ind[i], var[(ind[i]...,)...]) for i in eachindex(ind)])
    else
        return Dict{NTuple{ndims,Any},Any}([(ind[i], var[(ind[i]...,)...]) for i in eachindex(ind) if !ismissing(var[(ind[i]...,)...])])
    end
end

function convert_jump_container_to_dict(var::JuMP.Containers.SparseAxisArray; keepmissing::Bool=true)
    firstkey = first(keys(var.data))
    ndims = length(firstkey)
    if keepmissing
        return Dict{NTuple{ndims,Any},Any}([(k, v) for (k,v) in var.data])
    else
        return Dict{NTuple{ndims,Any},Any}([(k, v) for (k,v) in var.data if !ismissing(v)])
    end
end

function preceding_period(set,h)
    return circshift(set,1)[findfirst(==(h),set)]
end

function pass()
    return nothing
end

# Has limited use - since data would come from the same calendar year
function shift_hourset(dtr::DieterModel, shift_mode::Int=0)
    if shift_mode == 0
        shift = 0
    elseif shift_mode == 1 # Shift from Jan-Dec to Jul-Jun
        shift = -4344
    elseif shift_mode == -1 # Shift from Jul-Jun to Jan-Dec
        shift = +4344
    elseif abs(shift_mode) > 1
        shift = shift_mode
    else
        error("Invalid shift mode")
    end
    dtr.sets[:h] = circshift(dtr.sets[:h], shift)
    return nothing
end

function container_to_dense(var::JuMP.Containers.SparseAxisArray; dim_names::Union{Vector{Symbol},Nothing}=nothing, sets_map::Union{Dict{Symbol,Vector{String}},Nothing}=nothing)
    df = convert_jump_container_to_df(var, dim_names=dim_names)
    return convert_long_df_to_dense(df, sets_map; dims=dim_names)
end

function container_to_dense(var::JuMP.Containers.DenseAxisArray; dim_names::Union{Vector{Symbol},Nothing}=nothing, sets_map::Union{Dict{Symbol,Vector{String}},Nothing}=nothing)
    return var
end