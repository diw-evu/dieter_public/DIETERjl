function sets_check_and_load(dtr::DieterModel, path::AbstractString, feature::Symbol, symbol::Symbol, verbose::Bool=false)
    @assert haskey(dtr.mapping["symbols_map"][string(feature)], string(symbol)) "$symbol is not present in $feature in mapping.json."
    current_symbol = dtr.mapping["symbols_map"][string(feature)][string(symbol)]
    @assert haskey(dtr.metadata, current_symbol) "$current_symbol is not present in metadata.json."
    symb_data = dtr.metadata[current_symbol]
    if verbose
        file = joinpath(path, symb_data["file_name"]...)
        println("   Reading $file")
    end
    set_coords = CSV.read(joinpath(path, symb_data["file_name"]...), values; select=[1])[1]
    @assert !haskey(dtr.sets, symbol) "Set '$symbol' has already been loaded once before. Current feature: $feature. Sets are collected only once"
    dtr.sets[symbol] = string.(set_coords)
    dtr.sets_map[Symbol(current_symbol)] = symbol
end

function get_instructions(symbol_data)
    set_row = Symbol.(symbol_data["set_row"])
    if symbol_data["set_col"] === nothing
        set_col = symbol_data["set_col"]
    else
        set_col = Symbol(symbol_data["set_col"])
    end
    if symbol_data["col_loc"] == -1
        col_pos = symbol_data["col_loc"]
    else
        if symbol_data["col_loc"] !== nothing
            col_pos = symbol_data["col_loc"] + 1
        else
            col_pos = -1
        end
    end
    return set_row, set_col, col_pos
end

function collect_data(dtr::DieterModel, path::AbstractString, feature::Symbol, dense_or_sparse::Symbol, verbose::Bool=false)
    missing_symbols = []
    unneeded = []
    if verbose
        println(feature)
    end
    for (symbol, details) in dtr.symbols[feature]
        if details[:type] == "par"
            @assert haskey(dtr.mapping["symbols_map"][string(feature)], string(symbol)) "$symbol is not present in $feature in mapping.json."
            current_symbol = dtr.mapping["symbols_map"][string(feature)][string(symbol)]
            if haskey(dtr.metadata, current_symbol)
                if verbose
                    println("   => $symbol")
                end
                symb_data = dtr.metadata[current_symbol]
                set_row, set_col, col_pos = get_instructions(symb_data)
                if verbose
                    file = joinpath(path, symb_data["file_name"]...)
                    println("   Reading $file")
                end
                if set_col !== nothing
                    orders = reorder(set_row, set_col, col_pos)
                    wide_df = CSV.read(joinpath(path, symb_data["file_name"]...), DataFrame)
                    long_df = df_stack(wide_df, row_set_names=set_row, col_set_name=set_col, insert_col_set=col_pos)
                else
                    orders = set_row
                    long_df = CSV.read(joinpath(path, symb_data["file_name"]...), DataFrame)
                end
                if dense_or_sparse == :dense
                    dtr.parameters[symbol] = convert_long_df_to_dense(long_df, dtr.sets; dims=orders, value_name=:value, sets_map=dtr.sets_map)
                elseif dense_or_sparse == :sparse
                    dtr.parameters[symbol] = convert_long_df_to_sparse(long_df; dims=orders, value_name=:value)
                end
            else
                push!(missing_symbols, symbol)
            end
        end
    end
    if !isempty(missing_symbols)
        for (symbol, symb_data) in dtr.metadata
            if symb_data["type"] == "par"
                if haskey(dtr.mapping["tables_map"][string(feature)], string(symbol))
                    header = Symbol(dtr.mapping["tables_map"][string(feature)][string(symbol)])
                    set_row, set_col, col_pos = get_instructions(symb_data)
                    if verbose
                        file = joinpath(path, symb_data["file_name"]...)
                        println("   Reading $file")
                    end
                    df = CSV.read(joinpath(path, symb_data["file_name"]...), DataFrame)
                    if set_col !== nothing
                        orders = reorder(set_row, set_col, col_pos)
                        df = df_stack(df, row_set_names=set_row, col_set_name=set_col, insert_col_set=col_pos)
                    else
                        orders = set_row
                    end
                    unique_items = unique(df[!, header])
                    push!(unneeded, [(symbol, elem) for elem in unique_items]...)
                    without_header = [item for item in orders if !(item == header)]
                    with_value = copy(without_header)
                    push!(with_value, :value)
                    missing_iteration = copy(missing_symbols)
                    for symb in missing_iteration
                        param = dtr.mapping["symbols_map"][string(feature)][string(symb)]
                        if param in unique_items
                            if verbose
                                println("   => $symb")
                            end
                            small_df = filter([header] => x -> x == param, df)
                            long_df = small_df[!, with_value]
                            if dense_or_sparse == :dense
                                dtr.parameters[symb] = convert_long_df_to_dense(long_df, dtr.sets; dims=without_header, value_name=:value, sets_map=dtr.sets_map)
                            elseif dense_or_sparse == :sparse
                                dtr.parameters[symb] = convert_long_df_to_sparse(long_df; dims=without_header, value_name=:value)
                            end
                            deleteat!(missing_symbols, findall(x -> x == symb, missing_symbols))
                            deleteat!(unneeded, findall(x -> x == (symbol, param), unneeded))
                        end
                    end
                end
            end
        end
    end
    if verbose
        if !isempty(missing_symbols)
            println("   Absent symbols in input data:")
            for sym in missing_symbols
                println("       $sym")
            end
            println("   For absent symbols see self.symbols. Only derived parameters should be omitted.")
        end
        if !isempty(unneeded)
            println("   Redundant headers in input data:") # Find this symbol as key in metadata.json
            println("       (metadata key)  :  (column name)")
            for colu in unneeded
                println("       $(colu[1]) => $(colu[2])")
            end
            println("   For redundant headers see self.metadata to find the associated files.")
        end
    end
end

function collect_sets(dtr::DieterModel, path::AbstractString, feature::Symbol, verbose::Bool=false)
    for (symbol, details) in dtr.symbols[feature]
        if details[:type] == "set"
            sets_check_and_load(dtr, path, feature, symbol, verbose)
        end
    end
end

function convert_long_df_to_dense(df::DataFrame, sets::Dict{Symbol,Vector{String}}; dims::Union{Vector{Symbol},Nothing}=nothing, value_name::Symbol=:value, sets_map::Union{Dict{Symbol,Symbol},Nothing}=nothing)::Union{JuMP.Containers.DenseAxisArray,Float16,Float32,Float64,Int16,Int32,Int64}
    if dims === nothing
        col_names = names(df)
        deleteat!(col_names, findall(x -> x == value_name, col_names))
        dims = col_names
    else
        if isempty(dims) || dims[1] == :nothing   # without dimenssions, taking the unique element, index = 1
            return df[1, value_name]
        end
        @assert all(String(dim) in names(df) for dim in dims) "$dims. One or more dimenssions not found in dataframe headers."
        @assert String(value_name) in names(df) "'$value_name' not found in dataframe."
    end
    if isempty(dims) || dims[1] == :nothing   # without dimenssions, taking the unique element, index = 1
        return df[1, value_name]
    else
        param_dict = convert_long_df_to_dict(df, value_name=value_name)
        if sets_map === nothing
            set_collection = [sets[coord] for coord in dims]
        else
            set_collection = [sets[sets_map[coord]] for coord in dims]
        end
        indices = [k for k in Base.Iterators.product(set_collection...)][:]
        definite_dict = Dict{Tuple,Union{Missing,Float16,Float32,Float64,Int16,Int32,Int64}}()
        for idx in indices
            if !haskey(param_dict, idx)
                definite_dict[idx] = missing
            else
                definite_dict[idx] = param_dict[idx]
            end
        end
        nd_container_matrix = reshape([definite_dict[tb] for tb in indices], length.(set_collection)...)
        return DenseAxisArray(nd_container_matrix, set_collection...)
    end
end

function convert_long_df_to_sparse(df::DataFrame; dims::Union{Vector{Symbol},Nothing}=nothing, value_name::Symbol=:value)::Union{JuMP.Containers.SparseAxisArray,Float16,Float32,Float64,Int16,Int32,Int64}
    if dims === nothing
        col_names = names(df)
        deleteat!(col_names, findall(x -> x == value_name, col_names))
        dims = col_names
    else
        if isempty(dims) || dims[1] == :nothing   # without dimenssions, taking the unique element, index = 1
            return df[1, value_name]
        end
        @assert all(String(dim) in names(df) for dim in dims) "$dims. One or more dimenssions not found in dataframe headers."
        @assert String(value_name) in names(df) "'$value_name' not found in dataframe."
    end
    if isempty(dims) || dims[1] == :nothing   # without dimenssions, taking the unique element, index = 1
        return df[1, value_name]
    else
        param_dict = convert_long_df_to_dict(df, value_name=value_name)
        return SparseAxisArray(param_dict)
    end
end

function get_feature_map(dtr::DieterModel; symbol::Symbol, feature::Symbol)
    # println("Creating map for $feature - $symbol")
    dims = dtr.symbols[feature][symbol][:dims]
    other = nothing
    for dim in dims
        if dim != :n
            other = dim
        end
    end
    idx_other = findfirst(==(other), dims)
    idx_n = findfirst(==(:n), dims)
    order = [idx_n, idx_other]
    symb_dict = convert_jump_container_to_dict(dtr.parameters[symbol])
    container = Dict{String,Any}()
    map = Dict{String,Any}()
    for n in dtr.sets[:n]
        container[n] = Vector{String}()
    end
    for n in dtr.sets[:n]
        for elem in dtr.sets[other]
            val = [n, elem]
            right = [val[findfirst(==(i), order)] for i in [1, 2]]
            tup = (right...,)
            if haskey(symb_dict, tup)
                if symb_dict[tup] !== missing
                    if symb_dict[tup] != 0
                        push!(container[n], elem)
                    end
                end
            end
        end
        map[n] = unique(container[n])
    end
    return map
end

function update_map(dtr::DieterModel; feature::Symbol, category::Symbol, maps::Dict{String,Any})
    if !haskey(dtr.maps,feature)
        dtr.maps[feature] = Dict{Symbol, Dict{String,Vector{String}}}()
    end
    if !haskey(dtr.maps[feature], category)
        dtr.maps[feature][category] = Dict{String,Vector{String}}()
    end
    for (k,v) in maps
        if haskey(dtr.maps[feature][category], k)
            dtr.maps[feature][category][k] = append!(copy(dtr.maps[feature][category][k]), v)
        else
            if v isa Vector
                dtr.maps[feature][category][k] = v
            else
                dtr.maps[feature][category][k] = [v]
            end
        end
    end
    for n in collect(keys(dtr.maps[feature][category]))
        dtr.maps[feature][category][n] = unique(copy(dtr.maps[feature][category][n]))
    end
end

function check_all_symbols_in_dataframe_dict(dtr::DieterModel, df_dict::Dict{Symbol,DataFrame}; features::Vector{Symbol})
    missing_symbols = Dict()
    all_symbols = true
    for feature in features
        missing_symbols[feature] = Vector{Symbol}()
        for (symbol, _) in dtr.symbols[feature]
            if !haskey(df_dict, symbol)
                if symbol in derived_parameters
                    continue
                end
                all_symbols = false
                push!(missing_symbols[feature], symbol)
            end
        end
    end
    if !all_symbols
        println("Missing symbols in input data:")
        for feature in features
            for symbol in missing_symbols[feature]
                println("   $symbol")
            end
        end
    end
    return all_symbols, missing_symbols
end

function get_parameters_from_dataframe_dict(dtr::DieterModel, df_dict::Dict{Symbol,DataFrame}; feature::Symbol, dense_or_sparse::Symbol, verbose::Bool=false)
    if verbose
        println("   Loading parameters for $feature...")
    end
    for (symbol, _) in dtr.symbols[feature]
        if symbol in derived_parameters
            continue
        end
        if dtr.symbols[feature][symbol][:type] == "par"
            if verbose
                println("       $symbol")
            end
            if dense_or_sparse == :dense
                dtr.parameters[symbol] = convert_long_df_to_dense(df_dict[symbol], dtr.sets; dims=dtr.symbols[feature][symbol][:dims], value_name=:value)
            elseif dense_or_sparse == :sparse
                dtr.parameters[symbol] = convert_long_df_to_sparse(df_dict[symbol]; dims=dtr.symbols[feature][symbol][:dims], value_name=:value)
            end
        end
    end
end

function get_sets_from_dataframe_dict(dtr::DieterModel, df_dict::Dict{Symbol,DataFrame}; feature::Symbol, verbose::Bool=false)
    if verbose
        println("   Loading sets for $feature...")
    end
    for (symbol, _) in dtr.symbols[feature]
        if dtr.symbols[feature][symbol][:type] == "set"
            if verbose
                println("       $symbol")
            end
            df = df_dict[symbol]
            @assert !isempty(df) "Dataframe at $feature has no data for $symbol"
            @assert String(symbol) in names(df) "Dataframe at $feature does not contain header '$symbol'"
            dtr.sets[symbol] = df[!, symbol]
        end
    end
end

function get_dims(parameter::Union{AbstractString,String,Symbol})
    if parameter isa AbstractString || parameter isa String
        parameter = Symbol(parameter)
    end
    for md in keys(symbols)
        dc = symbols[md]
        if haskey(dc, parameter)
            return (true, dc[parameter][:dims])
        end
    end
    return (false, [])
end

function get_dimenssions(parameter::Union{AbstractString,String,Symbol})
    if parameter isa AbstractString || parameter isa String
        parameter = Symbol(parameter)
    end
    for md in keys(symbols)
        dc = symbols[md]
        if haskey(dc, parameter)
            return dc[parameter][:dims]
        end
    end
    throw("Parameter $parameter not found")
end


function load_timeseries(dtr::DieterModel, parameter::AbstractString, option::Union{AbstractString,String,Int}, folder_path::String)
    # validation
    sym = Symbol(parameter)
    exists = false
    modu = nothing
    for (modules, dc) in symbols
        if sym in keys(dc)
            exists = true
            modu = modules
            break
        end
    end
    @assert exists "$parameter does not exist as a parameter for DIETER"
    dims = symbols[modu][sym][:dims]

    df = CSV.read(joinpath(folder_path, "$(parameter).csv"), DataFrame)
    df = filter(["option"] => x -> x == option, df)
    df = select!(df, Not(:option))
    cols = names(df)
    @assert all([dim in cols for dim in string.(dims)])
    for dim in dims
        for value in unique(df[!, dim])
            @assert value in dtr.sets[dim] "$value not found in $dim"
        end
    end
    return convert_long_df_to_dict(df)
end

function iteration_table(path::AbstractString, dtr::DieterModel, timeseries_folder::Union{AbstractString,Nothing}=nothing)
    table = CSV.read(path, DataFrame)

    header_info = [:features, :category, :sets, :dtype]
    table_info = select!(deepcopy(table), header_info...)
    table_scen = select!(deepcopy(table), Not(header_info))
    dcinfo = Dict(idx => Dict(pairs(df)) for (idx, df) in pairs(eachrow(table_info)))
    dcscen = Dict(idx => Dict((parse(Int, string(k)), v) for (k, v) in pairs(df)) for (idx, df) in pairs(eachrow(table_scen)))

    dcinout = Dict{Int,Any}()
    for (idx, dc) in dcscen
        for (k, v) in dc
            @assert k > 0 "Iteration table scenario columns should start from 1 or above. Detected $k"
            if !haskey(dcinout, k)
                dcinout[k] = Dict()
            end
            dcinout[k][idx] = v
        end
    end

    case = Dict{Int,Any}(keys(dcinout) .=> nothing)
    for (run, IdxValuePair) in dcinout
        case[run] = Dict()
        for (idx, value_str) in IdxValuePair
            if !haskey(case[run], dcinfo[idx][:category])
                case[run][dcinfo[idx][:category]] = Dict()
            end
            typstr = dcinfo[idx][:dtype]
            typ = eval(Meta.parse("$typstr"))
            if missing === value_str
                case[run][dcinfo[idx][:category]][idx] = value_str
            else
                if !(String == typ)
                    if isa(value_str, AbstractString)
                        case[run][dcinfo[idx][:category]][idx] = parse(typ, value_str)
                    else
                        case[run][dcinfo[idx][:category]][idx] = typ(value_str)
                    end
                else
                    case[run][dcinfo[idx][:category]][idx] = value_str
                end
            end
        end
    end

    symbols_coords = Dict()
    for (idx, items) in dcinfo
        symbol = items[:features]
        if "parameter" == items[:category]
            if missing === items[:sets]
                coords = ()
            else
                coords = tuple(split(items[:sets], ":")...)
            end
            symbols_coords[idx] = Dict()
            symbols_coords[idx]["symbol"] = symbol
            symbols_coords[idx]["coords"] = coords
        elseif "timeseries" == items[:category]
            symbols_coords[idx] = Dict()
            symbols_coords[idx]["symbol"] = symbol
        elseif "metadata" == items[:category]
            symbols_coords[idx] = Dict()
            symbols_coords[idx]["symbol"] = symbol
        elseif "extension" == items[:category]
            coords = tuple(split(items[:sets], ":")...)
            symbols_coords[idx] = Dict()
            symbols_coords[idx]["symbol"] = symbol
            symbols_coords[idx]["coords"] = coords
        elseif "scaling" == items[:category]
            symbols_coords[idx] = Dict()
            symbols_coords[idx]["symbol"] = symbol
        else
            throw("Only four keys are supported: 'parameter', 'timeseries', 'metadata', 'extension', 'scaling'")
        end
    end

    # validation
    dims_memory = Dict()
    for (idx, details) in symbols_coords
        symbol = details["symbol"]
        if "parameter" == dcinfo[idx][:category]
            @assert symbol != "extensions" "Cannot use 'extensions' as parameter name. Iteration table consider this as a special case"
            if !haskey(dims_memory, symbol)
                ret, dims = get_dims(symbol)
                if ret
                    dims_memory[symbol] = dims
                else
                    throw("$symbol not found. Make sure the parameter exists or check for typos")
                end
            end
            dims = dims_memory[symbol]
            ndims = length(dims)
            if ndims > 0
                set_pos = [0 for _ in 1:ndims]
            else
                set_pos = [0]
            end
            @assert length(details["coords"]) == ndims
            for (ix, key) in enumerate(details["coords"])
                if Symbol(key) in dims
                    set_pos[ix] = ix
                elseif !(key in dtr.sets[dims[ix]])
                    throw("Key $key not present in $symbol at $(dims[ix])")
                end
            end
            symbols_coords[idx]["position"] = set_pos
            symbols_coords[idx]["dims"] = dims
        elseif "extension" == dcinfo[idx][:category]
            @assert length(details["coords"]) == 1
            node = details["coords"][1]
            if node in dtr.sets[:n]
                symbols_coords[idx]["position"] = [0]
            elseif node == "n"
                symbols_coords[idx]["position"] = [1]
            else
                throw("Key $key not present in $symbol at $(dims[ix])")
            end
        end
    end

    # cartesian product of sets
    for (idx, details) in symbols_coords
        symbol = details["symbol"]
        if "parameter" == dcinfo[idx][:category]
            if sum(details["position"]) > 0
                container = []
                for (ix, v) in enumerate(details["position"])
                    if v == 0
                        push!(container, [details["coords"][ix]])
                    else
                        symset = details["dims"][v]
                        push!(container, dtr.sets[symset])
                    end
                end
                coords_tuples = Base.Iterators.product(container...)
                symbols_coords[idx]["coords_tuples"] = vcat(coords_tuples...)
            end
        elseif "extension" == dcinfo[idx][:category]
            if sum(details["position"]) > 0
                coords_tuples = [(n,) for n in dtr.sets[:n]]
                symbols_coords[idx]["coords_tuples"] = coords_tuples
            end
        end
    end

    scenarios = Dict{Int,Any}(keys(dcinout) .=> nothing)
    for (scn, config) in case
        scenarios[scn] = Dict()
        for (cat, content) in config
            scenarios[scn][cat] = Dict()
            for (key, v) in content
                data = symbols_coords[key]
                sname = data["symbol"]
                if !(v === missing)
                    if cat == "parameter"
                        if length(data["coords"]) == 0
                            scenarios[scn][cat][sname] = v
                        else
                            if !haskey(scenarios[scn][cat], sname)
                                scenarios[scn][cat][sname] = Dict{Tuple,Any}()
                            end
                            if sum(data["position"]) > 0
                                for coo in data["coords_tuples"]
                                    scenarios[scn][cat][sname][coo] = v
                                end
                            else
                                coo = data["coords"]
                                scenarios[scn][cat][sname][coo] = v
                            end
                        end
                    elseif cat == "metadata"
                        scenarios[scn][cat][sname] = v
                    elseif cat == "timeseries"
                        scenarios[scn][cat][sname] = load_timeseries(dtr, sname, v, timeseries_folder)
                    elseif cat == "extension"
                        if !haskey(scenarios[scn][cat], sname)
                            scenarios[scn][cat][sname] = Dict{Tuple,Any}()
                        end
                        if sum(data["position"]) > 0
                            for coo in data["coords_tuples"]
                                scenarios[scn][cat][sname][coo] = v
                            end
                        else
                            coo = data["coords"]
                            scenarios[scn][cat][sname][coo] = v
                        end
                    elseif cat == "scaling"
                        scenarios[scn][cat][sname] = v
                    else
                        throw("Column category should only consider 'metadata', 'timeseries', 'parameter', 'extension', 'scaling'. Given: $cat")
                    end
                end
            end
        end
    end
    return scenarios
end

function set_model_instance(optimizer, model_type::String="Model")
    @assert model_type in ["Model", "direct_model"] "model_type should be 'Model' or 'direct_model'"
    if model_type == "Model"
        model = Model(optimizer)
    elseif model_type == "direct_model"
        @warn "Model type 'direct_model' can not save or printout the model. Useful for GAMS.optimizer for debugging. For example: MOI.get(m, GAMS.OriginalConstraintName('eq1'))"
        model = direct_model(optimizer())
    end
    return model
end

function prepare_scenario(dtr::DieterModel, scenarios::Dict, scen_id::Int, verbose::Bool=false)
    start_time = time()
    scenario = scenarios[scen_id]
    dtr_copy = deepcopy(dtr)
    if haskey(scenario, "extension")
        for (ext, data) in scenario["extension"]
            for (coords, value) in data
                @assert value in [0, 1] "Extension value should be 0 or 1. Given: $value"
                node = coords[1]
                dtr_copy.ext[Symbol(ext)][node] = value
                dtr_copy.parameters[:extensions][node, ext] = value
            end
        end
    end
    if haskey(scenario, "parameter")
        for (par, data) in scenario["parameter"]
            @assert par != "extensions" "Cannot use 'extensions' as parameter name. Iteration table consider this as a special case"
            if data isa Dict
                for (coords, value) in data
                    dtr_copy.parameters[Symbol(par)][coords...] = value
                end
            else
                dtr_copy.parameters[Symbol(par)] = data
            end
        end
    end
    if haskey(scenario, "timeseries")
        for (par, data) in scenario["timeseries"]
            for (coords, value) in data
                dtr_copy.parameters[Symbol(par)][coords...] = value
            end
        end
    end
    metadata = Dict{Symbol,Any}()
    if haskey(scenario, "metadata")
        for (k, v) in scenario["metadata"]
            metadata[Symbol(k)] = v
        end
    end
    if haskey(scenario, "scaling")
        for (k, v) in scenario["scaling"]
            @assert k in ["obj", "energy", "currency", "co2"]
            dtr_copy.scale_dict[Symbol(k)] = v
        end
    end
    end_time = time()
    time_diff = end_time - start_time
    if verbose
        println(">>>> Passed: $scen_id in $(round(time_diff, digits=2)) seconds")
    end
    return scen_id, dtr_copy, metadata
end

function scenario_optimization(
    scen_int::Int,
    m::JuMP.AbstractModel,
    dtr_copy::DieterModel,
    metadata::Dict,
    maxhours::Int,
    save_model::Bool,
    resultspath::AbstractString,
    custom_symbols::Union{Vector{Tuple{Symbol,Vector{Symbol},Symbol,Symbol}},Nothing}=nothing,
    results_compress::Union{Symbol,Nothing}=nothing,
    results_sparse::Bool=false,
    results_verbose::Bool=false,
)

    define_model!(m, dtr_copy, maxhours=maxhours, scen=scen_int)
    scen_folder = joinpath(resultspath, dtr_copy.settings[:name_scenario])
    output_path = joinpath(scen_folder, "solver_stdout.txt")
    mkpath(scen_folder)
    number_str = lpad(scen_int, 4, "0")
    # optimize
    println("\nScenario $(number_str): Solver started...")
    redirect_stdout((()->optimize!(m)), open(output_path, "w"))
   
    msg = "Scenario $(number_str): Termination status is: $(termination_status(m)). Objective function: $(objective_value(m))"
    println(msg)

    # Select variables and parameters to export
    if custom_symbols === nothing
        exog = [
            (:EnergyBalance, [:n, :h], :Marginal, :Constraint),
            (:Z, Vector{Symbol}(), :Value, :Objective),
        ]
        pars = collect_symbols(dtr_copy, m, :Parameter) # All parameters
        vars = collect_symbols(dtr_copy, m, :Variable)  # All variables
        symbs = [exog; pars; vars]
    else
        symbs = custom_symbols
    end

    collect_results(dtr_copy, m, symbs, results_sparse, results_verbose)
    save_scenario(dtr_copy, m, resultspath, custom_config=metadata, compress=results_compress, save_model=save_model)
    println("Scenario $(number_str): Done!\n")
    return msg
end