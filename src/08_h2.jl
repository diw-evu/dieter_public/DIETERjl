function cond_h2(tech::AbstractString, node::AbstractString, dtr::DieterModel)
  if tech in dtr.maps[:h2][:base][node]
      return true
  else
      return false
  end
end

function cond_h2_net(arc::AbstractString, dtr::DieterModel)
    test = []
    for node in dtr.sets[:n]
        if arc in dtr.maps[:h2][:net][node]
            push!(test,true)
        else
            push!(test,false)
        end
    end
    if arc in dtr.sets[:connectorsh2]
        return sum(test) == 2
    elseif arc in dtr.sets[:importsh2]
        return sum(test) == 1
    else
        return false
    end
end

function calc_h2_parameters!(dtr::DieterModel)
    N   = dtr.sets[:n]
    TechEly = dtr.sets[:elyh2]
    StoH2   = dtr.sets[:stoh2]
    ReconH2 = dtr.sets[:reconh2]

    dtr.parameters[:h2_investment_cost_ely]   = round.(DenseAxisArray([cond_h2(t,n,dtr) ? dtr.parameters[:h2_overnight_cost_ely][n,t]  * annuity(dtr.parameters[:node_interest][n],dtr.parameters[:h2_lifetime_ely][n,t])   : missing for n in N, t in TechEly], N, TechEly), digits=3)
    dtr.parameters[:h2_investment_cost_comp]  = round.(DenseAxisArray([cond_h2(t,n,dtr) ? dtr.parameters[:h2_overnight_cost_comp][n,t] * annuity(dtr.parameters[:node_interest][n],dtr.parameters[:h2_lifetime_comp][n,t])  : missing for n in N, t in TechEly], N, TechEly), digits=3)
    dtr.parameters[:h2_investment_cost_sto]   = round.(DenseAxisArray([cond_h2(s,n,dtr) ? dtr.parameters[:h2_overnight_cost_sto][n,s]  * annuity(dtr.parameters[:node_interest][n],dtr.parameters[:h2_lifetime_sto][n,s])   : missing for n in N, s in StoH2  ], N, StoH2), digits=3)
    dtr.parameters[:h2_investment_cost_recon] = round.(DenseAxisArray([cond_h2(r,n,dtr) ? dtr.parameters[:h2_overnight_cost_recon][n,r]* annuity(dtr.parameters[:node_interest][n],dtr.parameters[:h2_lifetime_recon][n,r]) : missing for n in N, r in ReconH2], N, ReconH2), digits=3)
    return nothing
end

function sanity_check_h2_year(dtr::DieterModel)
    for (node, binary) in dtr.ext[:h2]
        if binary == 1
            if dtr.parameters[:node_hydrogen_yr_demand][node] != -1
                @assert dtr.parameters[:node_hydrogen_hr_fraction][node] >= 1/8760 "node_hydrogen_hr_fraction must be at least $(1/8760) for every node. Node: $node, Fraction: $(dtr.parameters[:node_hydrogen_hr_fraction][node])"
            end
        end
    end
end


function h2_module(m::JuMP.Model,dtr::DieterModel,obj_expr, ebal, min_res_expr, corr_factor, scale_dict)
    update_map(dtr;feature=:h2, category=:base, maps=get_feature_map(dtr; symbol=:h2_ely_enabled, feature=:h2))
    update_map(dtr;feature=:h2, category=:base, maps=get_feature_map(dtr; symbol=:h2_sto_enabled, feature=:h2))
    update_map(dtr;feature=:h2, category=:base, maps=get_feature_map(dtr; symbol=:h2_recon_enabled, feature=:h2))
    update_map(dtr;feature=:h2, category=:net, maps=get_feature_map(dtr; symbol=:h2_incidence, feature=:h2))
    calc_h2_parameters!(dtr)


    Nodes   = dtr.sets[:n]
    Hours   = dtr.sets[:h]
    TechEly = dtr.sets[:elyh2]
    StoH2   = dtr.sets[:stoh2]
    ReconH2 = dtr.sets[:reconh2]
    ArcsH2 = dtr.sets[:arcsh2]
    ConnectorsH2 = dtr.sets[:connectorsh2]
    ImportsH2 = dtr.sets[:importsh2]


    # Scaling factors
    sf_ener = !isnothing(scale_dict) ? scale_dict[:energy] : 1.0
    sf_curr = !isnothing(scale_dict) ? scale_dict[:currency] : 1.0


    MinRES               = dtr.parameters[:node_min_res_share]
    h2_dispatch_fraction = container_to_dense(dtr.parameters[:node_hydrogen_hr_fraction]; dim_names=get_dimenssions(:node_hydrogen_hr_fraction), sets_map=dtr.sets)

    # parameters
    d_h2_year         = dtr.parameters[:node_hydrogen_yr_demand]*sf_ener
    d_h2_hourly       = dtr.parameters[:h2_hydrogen_hr_demand]*sf_ener

    InvestmentCost_h2_ely   = dtr.parameters[:h2_investment_cost_ely]*sf_curr
    FixedCost_h2_ely        = dtr.parameters[:h2_fixed_cost_ely]*sf_curr
    Efficiency_h2_ely       = dtr.parameters[:h2_efficiency_ely]
    avail_h2_ely            = dtr.parameters[:h2_availability_ely]

    h2_max_power_ely        = dtr.parameters[:h2_max_power_ely]*sf_ener
    h2_min_power_ely        = dtr.parameters[:h2_min_power_ely]*sf_ener

    InvestmentCost_h2_comp  = dtr.parameters[:h2_investment_cost_comp]*sf_curr
    MarginalCost_h2_comp    = dtr.parameters[:h2_compression_variable_cost]*sf_curr
    FixedCost_h2_comp       = dtr.parameters[:h2_fixed_cost_comp]*sf_curr
    Efficiency_h2_comp      = dtr.parameters[:h2_efficiency_comp]
    h2_comp_ed_ratio        = dtr.parameters[:h2_compression_electrical_demand]

    InvestmentCost_h2_sto   = dtr.parameters[:h2_investment_cost_sto]*sf_curr
    FixedCost_h2_sto        = dtr.parameters[:h2_fixed_cost_sto]*sf_curr
    StaticEfficiency        = dtr.parameters[:h2_static_efficiency_sto]
    avail_h2_sto            = dtr.parameters[:h2_availability_sto]
    phi_h2_sto_min          = dtr.parameters[:h2_level_min_sto]

    h2_max_energy_sto       = dtr.parameters[:h2_max_energy_sto]*sf_ener
    h2_min_energy_sto       = dtr.parameters[:h2_min_energy_sto]*sf_ener

    InvestmentCost_h2_recon = dtr.parameters[:h2_investment_cost_recon]*sf_curr
    MarginalCost_h2_recon   = dtr.parameters[:h2_reconversion_variable_cost]*sf_curr
    FixedCost_h2_recon      = dtr.parameters[:h2_fixed_cost_recon]*sf_curr
    h2_max_power_recon      = dtr.parameters[:h2_max_power_recon]*sf_ener
    h2_min_power_recon      = dtr.parameters[:h2_min_power_recon]*sf_ener
    h2_efficiency_recon     = dtr.parameters[:h2_efficiency_recon]

    h2_max_flow             = dtr.parameters[:h2_max_flow]*sf_ener
    H2IncidenceMatrixImports= dtr.parameters[:h2_incidence_import]
    H2IncidenceMatrixExports= dtr.parameters[:h2_incidence_export]
    h2_arc_efficiencies     = dtr.parameters[:h2_arc_efficiency]
    H2ImportCosts           = dtr.parameters[:h2_import_costs]*sf_curr
    h2_flow_comp_demand     = dtr.parameters[:h2_flow_comp_demand] # TODO: Should be multiplied/divided by sf_ener as converts ton H2 to electricity consumption

    h2_infeas_cost          = dtr.parameters[:infeas_h2_cost]*sf_curr

    # variables for module
    @variables(m, begin
      H2_ELY_IN[n=Nodes,t=TechEly,h=Hours;      cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_ely[n,t] != 0] >=0
      H2_N_ELY[n=Nodes,t=TechEly;               cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_ely[n,t] != 0] >=0
      H2_STO_IN[n=Nodes,s=StoH2,h=Hours;        cond_ext(:h2,n,dtr) && cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0] >=0
      H2_STO_L[n=Nodes,s=StoH2,h=Hours;         cond_ext(:h2,n,dtr) && cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0] >=0
      H2_STO_OUT[n=Nodes,s=StoH2,h=Hours;       cond_ext(:h2,n,dtr) && cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0] >=0
      H2_N_STO_E[n=Nodes,s=StoH2;               cond_ext(:h2,n,dtr) && cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0] >=0
      H2_STO_OUT_DIR[n=Nodes,s=StoH2,h=Hours;   cond_ext(:h2,n,dtr) && cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0] >=0
      H2_N_RECON[n=Nodes,r=ReconH2;             cond_ext(:h2,n,dtr) && cond_h2(r,n,dtr) && h2_max_power_recon[n,r] != 0] >=0
      H2_RECON_OUT[n=Nodes,r=ReconH2,h=Hours;   cond_ext(:h2,n,dtr) && cond_h2(r,n,dtr) && h2_max_power_recon[n,r] != 0] >=0
      H2_FLOW[a=ArcsH2,h=Hours;                 cond_h2_net(a,dtr) && h2_max_flow[a] !=0] >=0
      H2_DIR_DEM[n=Nodes,h=Hours;               cond_ext(:h2,n,dtr)] >=0
      H2_IMP[a=ImportsH2;                       cond_h2_net(a,dtr) && h2_max_flow[a] !=0] >=0
      H2_INF[n=Nodes,h=Hours;                   cond_ext(:h2,n,dtr)] >=0
      H2_SPILL[n=Nodes,h=Hours;                 cond_ext(:h2,n,dtr)] >=0
    end);

    # Investment bounds

    for n=Nodes, t=TechEly
        if cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_ely[n,t] != 0
            if h2_max_power_ely[n,t] > 0
               set_upper_bound(H2_N_ELY[n,t],h2_max_power_ely[n,t])
            end
            if h2_min_power_ely[n,t] > 0
               set_lower_bound(H2_N_ELY[n,t],h2_min_power_ely[n,t])
            end
        end
    end

    for n=Nodes, sto=StoH2
        if cond_ext(:h2,n,dtr) && cond_h2(sto,n,dtr) && h2_max_energy_sto[n,sto] != 0
            if h2_max_energy_sto[n,sto] > 0
               set_upper_bound(H2_N_STO_E[n,sto],h2_max_energy_sto[n,sto])
            end
            if h2_min_energy_sto[n,sto] > 0
               set_lower_bound(H2_N_STO_E[n,sto],h2_min_energy_sto[n,sto])
            end
        end
    end

    for n=Nodes, recon=ReconH2
        if cond_ext(:h2,n,dtr) && cond_h2(recon,n,dtr) && h2_max_power_recon[n,recon] != 0
            if h2_max_power_recon[n,recon] > 0
               set_upper_bound(H2_N_RECON[n,recon],h2_max_power_recon[n,recon])
            end
            if h2_min_power_recon[n,recon] > 0
               set_lower_bound(H2_N_RECON[n,recon],h2_min_power_recon[n,recon])
            end
        end
    end

    adj_factor = corr_factor>0 ? 1/corr_factor : 0
    # Modify objective function
    add_to_expression!(obj_expr,
        # marginal costs of compression and reconversion
          adj_factor*(sum(MarginalCost_h2_comp[n,t]*Efficiency_h2_ely[n,t]*Efficiency_h2_comp[n,t]*H2_ELY_IN[n,t,h] for n in Nodes,t in TechEly, h in Hours if cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_ely[n,t] != 0; init=0)
        + sum(MarginalCost_h2_recon[n,t]*H2_RECON_OUT[n,t,h] for n in Nodes, t in ReconH2, h in Hours if cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_recon[n,t] != 0; init=0)
          )
        + (
        # Electrolysis Capex
        + sum((InvestmentCost_h2_ely[n,t] + FixedCost_h2_ely[n,t])*H2_N_ELY[n,t] for n in Nodes,t in TechEly if cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_ely[n,t] != 0; init=0) 
        # Compression Capex
        + sum((InvestmentCost_h2_comp[n,t] + FixedCost_h2_comp[n,t])*Efficiency_h2_ely[n,t]*H2_N_ELY[n,t] for n in Nodes, t in TechEly if cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_ely[n,t] != 0; init=0)
        # H2 Storage Capex
        + sum((InvestmentCost_h2_sto[n,s] + FixedCost_h2_sto[n,s])*H2_N_STO_E[n,s] for n in Nodes, s in StoH2 if cond_ext(:h2,n,dtr) && cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0; init=0) 
        # H2 Reconversion 
        + sum((InvestmentCost_h2_recon[n,t] + FixedCost_h2_recon[n,t])*H2_N_RECON[n,t] for n in Nodes, t in ReconH2 if cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_recon[n,t] != 0; init=0)
        # H2 Import
        + sum(H2ImportCosts[a]*H2_FLOW[a,h] for a in ImportsH2, h in Hours if cond_h2_net(a,dtr) && h2_max_flow[a] != 0; init=0)
        )
        # Costs of h2 load shedding
        + sum(h2_infeas_cost*H2_INF[n,h] for n in Nodes, h in Hours if cond_ext(:h2,n,dtr); init=0)
    );

    # Modify energy balance and minimum renewables constraint
    for n in Nodes
        if length(Hours) > 0
            for h in Hours
                add_to_expression!(ebal[n,h],
                    # demand side
                    - sum(H2_ELY_IN[n,t,h]*(1+Efficiency_h2_ely[n,t]*Efficiency_h2_comp[n,t]*h2_comp_ed_ratio[n,t]) for t in TechEly if cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_ely[n,t] != 0;init=0)
                    - sum(H2IncidenceMatrixExports[n,a]*H2_FLOW[a,h]*h2_flow_comp_demand[a] for a in ConnectorsH2 if cond_h2_net(a,dtr) && h2_max_flow[a]!=0; init=0)
                    # supply side
                    + sum(H2_RECON_OUT[n,t,h] for t in ReconH2 if cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_recon[n,t] != 0;init=0)
                );
            end
        end
        if MinRES[n]>0 && cond_ext(:h2,n,dtr)
            add_to_expression!(min_res_expr[n],
            # Here we separate losses and hydrogen demand. Then losses are coveres 100% res, and hydrogen demand (electric) percentage RES
            # This line apply when using hourly hydrogen demand
            + MinRES[n]*sum(d_h2_hourly[n,h] for h in Hours if cond_ext(:h2,n,dtr) && d_h2_year[n] == -1; init=0)
            # This line apply when using yearly hydrogen demand
            + MinRES[n]*sum(d_h2_year[nn] * corr_factor for nn in [n] if cond_ext(:h2,nn,dtr) && d_h2_year[nn] != -1; init=0)
            # Usually losses are covered 100% by RES to avoid USC
            # This line apply when using hourly hydrogen demand
            + sum(H2_ELY_IN[n,t,h]*(1+Efficiency_h2_ely[n,t]*Efficiency_h2_comp[n,t]*h2_comp_ed_ratio[n,t]) - H2_RECON_OUT[n,r,h] - d_h2_hourly[n,h] for t in TechEly, r in ReconH2, h in Hours if cond_ext(:h2,n,dtr) && d_h2_year[n] == -1; init=0)
            # This line apply when using yearly hydrogen demand
            + sum(H2_ELY_IN[n,t,h]*(1+Efficiency_h2_ely[n,t]*Efficiency_h2_comp[n,t]*h2_comp_ed_ratio[n,t]) - H2_RECON_OUT[n,r,h] for t in TechEly, r in ReconH2, h in Hours if cond_ext(:h2,n,dtr) && d_h2_year[n] == -1; init=0) - sum(d_h2_year[nn] * corr_factor for nn in [n] if cond_ext(:h2,nn,dtr) && d_h2_year[nn] != -1; init=0)
            );
        end
    end # TO DO: Think about electricity demand from transport compressors in min res constraint

    # H2 Energy balance
    @constraint(m,H2Ebal[n=Nodes,h=Hours; cond_ext(:h2,n,dtr)],
        # supply
            sum(H2_ELY_IN[n,t,h]*Efficiency_h2_ely[n,t]*Efficiency_h2_comp[n,t] for t in TechEly if cond_h2(t,n,dtr) && h2_max_power_ely[n,t] != 0; init=0)
        +   sum(H2_STO_OUT[n,s,h] for s in StoH2 if cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0; init=0)
        +   H2_INF[n,h]
        # demand
        -   sum((H2IncidenceMatrixExports[n,a]+h2_arc_efficiencies[a]*H2IncidenceMatrixImports[n,a])*H2_FLOW[a,h] for a in ArcsH2 if h2_max_flow[a] != 0 && cond_h2_net(a,dtr); init=0) # Add efficiency factor to incidence matrix
        -   sum(H2_STO_IN[n,s,h] for s in StoH2 if cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0; init=0)
        -   sum(H2_RECON_OUT[n,r,h]/h2_efficiency_recon[n,r] for r in ReconH2 if cond_h2(r,n,dtr) && h2_max_power_recon[n,r] != 0; init=0)
        -   H2_DIR_DEM[n,h]
        -   H2_SPILL[n,h]
        == 0
    );


    # Maximum electrolyzer input
    @constraint(m,ElyInput[n=Nodes,t=TechEly,h=Hours; cond_ext(:h2,n,dtr) && cond_h2(t,n,dtr) && h2_max_power_ely[n,t] != 0],
        H2_ELY_IN[n,t,h] <= avail_h2_ely[n,t]*H2_N_ELY[n,t]
    );

    # H2 Storage plant equation
    @constraint(m,H2StoPlant[n=Nodes,s=StoH2,h=Hours; cond_ext(:h2,n,dtr) && cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0],
          H2_STO_L[n,s,h] 
        ==
          StaticEfficiency[n,s]*H2_STO_L[n,s,circshift(Hours,1)[findfirst(==(h),Hours)]]
        + H2_STO_IN[n,s,h]
        - H2_STO_OUT[n,s,h]
    );

    # H2 direct demand balance
    @constraint(m,H2EBal_hourly[n=Nodes,h=Hours; cond_ext(:h2,n,dtr) && d_h2_year[n] == -1],
        H2_DIR_DEM[n,h] == d_h2_hourly[n,h] 
    ); # If hourly profile is provided


    @constraint(m,H2EBal_overall[n=Nodes; cond_ext(:h2,n,dtr) && d_h2_year[n] != -1],
        sum(H2_DIR_DEM[n,h] for h in Hours) == d_h2_year[n] * corr_factor
    );

    @constraint(m,H2_dispatch_cap[n=Nodes,h=Hours; cond_ext(:h2,n,dtr) && d_h2_year[n] != -1],
        H2_DIR_DEM[n,h] <= d_h2_year[n] * h2_dispatch_fraction[n]
    ); # note that friction needs to be weakly larger than d_h2_year[n]/8760

    # Maximum storage level
    @constraint(m,MaxH2StoLevel[n=Nodes,s=StoH2,h=Hours; cond_ext(:h2,n,dtr) && cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0],
        H2_STO_L[n,s,h] <= avail_h2_sto[n,s]*H2_N_STO_E[n,s]
    );

    # Minimum storage level
    @constraint(m,MinH2StoLevel[n=Nodes,s=StoH2,h=Hours; cond_ext(:h2,n,dtr) && cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0],
      H2_STO_L[n,s,h] >= H2_N_STO_E[n,s]*phi_h2_sto_min[n,s]  # *avail_h2_sto[n,s] # I do not consider a availability de-rating on a minimum constraint useful 
    );

    # Maximum flow on connecting pipelines imports constrained on annual basis
    for a=ConnectorsH2, h=Hours
        if cond_h2_net(a,dtr) && h2_max_flow[a]!=0
            if h2_max_flow[a] > 0
               set_upper_bound(H2_FLOW[a,h],h2_max_flow[a])
            end
        end
    end


    # Define h2 import contracts
    # 1. Physical constraints - cannot import more than the capacity of the pipeline or terminal permits
    # need an annual constraint for decomposed problem (where master decisions are not made on an hourly basis)
    for a=ImportsH2
        if cond_h2_net(a,dtr) && h2_max_flow[a]!=0
            set_upper_bound(H2_IMP[a],h2_max_flow[a]*8760/(1e6/sf_ener))
        end
    end

    # 2. Baseload constraint (allowing for 10% daily flex in imports - to be parameterised later)
    @constraint(m,DailyMaxH2Import[a=ImportsH2,h=Hours; cond_h2_net(a,dtr) && h2_max_flow[a]!=0],
        H2_FLOW[a,h] <= H2_IMP[a]*(1e6*sf_ener)/8760*(1+0.1)
    );

    # 3. Annual contract quantities

    @constraint(m,AnnualH2Import[a=ImportsH2; cond_h2_net(a,dtr) && h2_max_flow[a]!=0],
        sum(H2_FLOW[a,h] for h in Hours) == H2_IMP[a]*corr_factor*1e6*sf_ener
    );


    # Imports are restricted to baseload contracts, which is why the capacity binds on an annual basis (TWh/a)
    #@constraint(m,DailyH2Imports[a=ImportsH2,h=Hours; cond_h2_net(a,dtr) && h2_max_flow[a]!=0],
    #    H2_FLOW[a,h]/1e6 == H2_IMP[a]*corr_factor/length(Hours)
    #);



    #@constraint(m,H2Recon[n=Nodes,h=Hours; cond_ext(:h2,n,dtr)],
    #  sum(H2_STO_OUT[n,s,h] for s in StoH2 if cond_h2(s,n,dtr) && h2_max_energy_sto[n,s] != 0;init=0)
    #  ==
    #  sum(H2_RECON_OUT[n,r,h]/h2_efficiency_recon[n,r] for r in ReconH2 if cond_h2(r,n,dtr) && h2_max_power_recon[n,r] != 0;init=0)
    #);

    @constraint(m,H2ReconMax[n=Nodes,r=ReconH2,h=Hours; cond_ext(:h2,n,dtr) && cond_h2(r,n,dtr) && h2_max_power_recon[n,r] != 0],
      H2_RECON_OUT[n,r,h] <= H2_N_RECON[n,r]
    );

    @constraint(m, H2_INF_Disabled[n=Nodes,h=Hours; cond_ext(:h2,n,dtr) && h2_infeas_cost==-1],
        H2_INF[n,h] == 0
        );

end
