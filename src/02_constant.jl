#  KEEP UPDATED: core_modules, extension_modules, AND symbols
core_modules      = [:scalars, :node, :gen, :sto, :net, :ext]
extension_modules = [:ev, :h2, :heat]

ssets = Dict{Symbol, Symbol}(
  :h_set => :h,
  :inflow => :sto,
  :ren => :gen,
  :con => :gen,
  :dis => :gen,
  :nondis => :gen,
  :connectorsh2 => :arcsh2,
  :importsh2 => :arcsh2,
  # :stoch => :gen
)

derived_parameters = [
  :gen_investment_cost, 
  :gen_marginal_cost, 
  :sto_investment_cost_energy,
  :sto_investment_cost_power_in,
  :sto_investment_cost_power_out,
  :h2_investment_cost_ely,
  :h2_investment_cost_comp,
  :h2_investment_cost_sto,
  :h2_investment_cost_recon,
  :ev_swap_investment_cost_energy,
  :ev_swap_investment_cost_power,
]

symbols = Dict{Symbol, Dict{Symbol, Dict{Symbol, Any}}}(
        :scalars => Dict([
            (:infeas_cost, Dict{Symbol, Any}(:type => "par", :ndims => 0, :dims => Vector{Symbol}())),
            (:infeas_ev_cost, Dict{Symbol, Any}(:type => "par", :ndims => 0, :dims => Vector{Symbol}())),
            (:infeas_h2_cost, Dict{Symbol, Any}(:type => "par", :ndims => 0, :dims => Vector{Symbol}())),

        ]),
        :node => Dict([
            (:n,        Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:h,        Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:node_electricity_demand,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:h])),
            (:node_co2_cap,                             Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:n])),
            (:node_min_res_share,                       Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:n])),
            (:node_interest,                            Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:n])),
            (:node_co2_price,                           Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:n])),
            (:node_ev_quantity,                         Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:n])),
            (:node_hydrogen_yr_demand,                  Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:n])),
            (:node_hydrogen_hr_fraction,                Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:n])),
            (:node_ev_discharge_cap_share,              Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:n])),
            (:node_ev_charge_cap_share,                 Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:n])),
        ],),
        :gen => Dict([
            (:gen,      Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:ren,      Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:con,      Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:dis,      Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:nondis,   Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            # (:stoch,    Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:gen_overnight_cost,                       Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_lifetime,                             Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_investment_cost,                      Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_fuel_cost,                            Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_efficiency,                           Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_variable_cost,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_carbon_content,                       Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_marginal_cost,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_fixed_cost,                           Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_availability,                         Dict{Symbol, Any}(:type => "par", :ndims => 3, :dims => [:n,:gen,:h])),
            (:gen_max_power,                            Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_min_power,                            Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_max_energy,                           Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
            (:gen_tech_enabled,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:gen])),
        ],),
        :sto => Dict([
            (:sto,      Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:inflow,   Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:sto_overnight_cost_energy,                Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_overnight_cost_power_in,              Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_overnight_cost_power_out,             Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_lifetime,                             Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_investment_cost_energy,               Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_investment_cost_power_in,             Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_investment_cost_power_out,            Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_fixed_cost_energy,                    Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_fixed_cost_power_in,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_fixed_cost_power_out,                 Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_inflow,                               Dict{Symbol, Any}(:type => "par", :ndims => 3, :dims => [:n,:sto,:h])),
            (:sto_max_power_in,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_max_power_out,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_min_power_in,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_min_power_out,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_min_energy,                           Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_max_energy,                           Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_efficiency_in,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_efficiency_out,                       Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_static_efficiency,                    Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_etop_max,                             Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_marginal_cost,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
            (:sto_tech_enabled,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:sto])),
        ],),
        :net => Dict([
            (:l,        Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:net_max_flow,                             Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:l])),
            (:net_efficiency,                           Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:l])),
            (:net_incidence_matrix,                     Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:l])),
        ],),
        :ext => Dict([
            # (:ext,      Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:extensions,                               Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ext])),
        ],),        
        :ev => Dict([
            (:ev,       Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:swap,     Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:ev_share,                                 Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_catenary_enabled,                      Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_exogenous_enabled,                     Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_discharge_enabled,                     Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_min_soc,                               Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_max_soc,                               Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_battery_capacity,                      Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_marginal_cost_charge,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_marginal_cost_discharge,               Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_marginal_cost_direct,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_efficiency_charge,                     Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_efficiency_discharge,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_driving_consumption,                   Dict{Symbol, Any}(:type => "par", :ndims => 3, :dims => [:n,:ev,:h])),
            (:ev_station_power_rating,                  Dict{Symbol, Any}(:type => "par", :ndims => 3, :dims => [:n,:ev,:h])),
            (:ev_bidirectional_time,                    Dict{Symbol, Any}(:type => "par", :ndims => 3, :dims => [:n,:ev,:h])),
            (:ev_grid_electricity_demand,               Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev,:h])),
            (:ev_profile_enabled,                       Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),

            (:ev_charge_enabled,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_swap_enabled,                          Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:ev])),
            (:ev_swap_time,                             Dict{Symbol, Any}(:type => "par", :ndims => 4, :dims => [:n,:ev,:swap,:h])),
            (:ev_swap_station_enabled,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_overnight_cost_energy,            Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_overnight_cost_power,             Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_lifetime,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_investment_cost_energy,           Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_investment_cost_power,            Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_marginal_cost,                    Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_fixed_cost_energy,                Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_fixed_cost_power,                 Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_efficiency_charge,                Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_efficiency_static,                Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
            (:ev_swap_full_premium,                     Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:swap])),
        ],),
        :h2 => Dict([
            (:elyh2,    Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:stoh2,    Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:reconh2,  Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),

            (:h2_hydrogen_hr_demand,                    Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:h])),

            (:h2_ely_enabled,                           Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_compression_variable_cost,             Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_overnight_cost_ely,                    Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_overnight_cost_comp,                   Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_lifetime_ely,                          Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_lifetime_comp,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_investment_cost_ely,                   Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_investment_cost_comp,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_fixed_cost_ely,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_fixed_cost_comp,                       Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_efficiency_ely,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_efficiency_comp,                       Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_compression_electrical_demand,         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_availability_ely,                      Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_min_power_ely,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),
            (:h2_max_power_ely,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:elyh2])),

            (:h2_sto_enabled,                           Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),
            (:h2_overnight_cost_sto,                    Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),
            (:h2_lifetime_sto,                          Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),
            (:h2_investment_cost_sto,                   Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),
            (:h2_fixed_cost_sto,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),
            (:h2_static_efficiency_sto,                 Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),
            (:h2_availability_sto,                      Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),
            (:h2_level_min_sto,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),
            (:h2_min_energy_sto,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),
            (:h2_max_energy_sto,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:stoh2])),

            (:h2_recon_enabled,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:reconh2])),
            (:h2_reconversion_variable_cost,            Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:reconh2])),
            (:h2_overnight_cost_recon,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:reconh2])),
            (:h2_lifetime_recon,                        Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:reconh2])),
            (:h2_investment_cost_recon,                 Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:reconh2])),
            (:h2_fixed_cost_recon,                      Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:reconh2])),
            (:h2_min_power_recon,                       Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:reconh2])),
            (:h2_max_power_recon,                       Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:reconh2])),
            (:h2_efficiency_recon,                      Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:reconh2])),

            (:arcsh2,        Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:connectorsh2,  Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:importsh2,     Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:h2_incidence_import,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:arcsh2])),
            (:h2_incidence_export,                  Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:arcsh2])),
            (:h2_incidence,                         Dict{Symbol, Any}(:type => "par", :ndims => 2, :dims => [:n,:arcsh2])),
            (:h2_max_flow,                          Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:arcsh2])),
            (:h2_arc_efficiency,                    Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:arcsh2])),
            (:h2_flow_comp_demand,                  Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:arcsh2])),
            (:h2_import_costs,                      Dict{Symbol, Any}(:type => "par", :ndims => 1, :dims => [:arcsh2])),
            # here add more parameters
        ],),
        :heat => Dict([
          # this is an example as the heat module not ready yet
            (:heat_sink,     Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:heat_building, Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),
            (:heat_tech,     Dict{Symbol, Any}(:type => "set", :ndims => nothing, :dims => nothing)),

            (:heat_enabled,                             Dict{Symbol, Any}(:type => "par", :ndims => 4, :dims => [:n,:heat_sink,:heat_building,:heat_tech])),
            (:heat_dc_demand,                           Dict{Symbol, Any}(:type => "par", :ndims => 4, :dims => [:n,:heat_sink,:heat_building,:h])),
            (:heat_cop,                                 Dict{Symbol, Any}(:type => "par", :ndims => 4, :dims => [:n,:heat_tech,:heat_sink,:h])),
            (:heat_share,                               Dict{Symbol, Any}(:type => "par", :ndims => 4, :dims => [:n,:heat_sink,:heat_building,:heat_tech])),
            (:heat_storage_efficiency,                  Dict{Symbol, Any}(:type => "par", :ndims => 4, :dims => [:n,:heat_tech,:heat_building,:heat_sink])),
            (:heat_storage_static_efficiency,           Dict{Symbol, Any}(:type => "par", :ndims => 4, :dims => [:n,:heat_tech,:heat_building,:heat_sink])),
            (:heat_storage_duration,                    Dict{Symbol, Any}(:type => "par", :ndims => 4, :dims => [:n,:heat_tech,:heat_building,:heat_sink])),
            # here add more parameters
        ],),
    )


####################################
######                       #######
###### Loading data mappings #######
######                       #######
####################################

mapping = Dict{String, Dict{String, Dict{String,String}}}(
# MODULE > MODEL SET OR PARAMETER > INPUT DATA (IT COULD BE PRESENT AS METADATA KEY)
"symbols_map" => Dict{String, Dict{String,String}}(
    "scalars" => Dict{String,String}(
        "infeas_cost" => "infeas_cost",
        "infeas_ev_cost" => "infeas_ev_cost",
        "infeas_h2_cost" => "infeas_h2_cost"
    ),
    "node" => Dict{String,String}(
        "h"                         => "h",
        "n"                         => "n",
        "node_interest"             => "node_interest",
        "node_min_res_share"        => "node_min_res_share",
        "node_co2_price"            => "node_co2_price",
        "node_co2_cap"              => "node_co2_cap",
        "node_electricity_demand"   => "node_electricity_demand",
        "node_ev_quantity"          => "node_ev_quantity",
        "node_hydrogen_yr_demand"   => "node_hydrogen_yr_demand",
        "node_hydrogen_hr_fraction" => "node_hydrogen_hr_fraction",
        "node_ev_discharge_cap_share" => "node_ev_discharge_cap_share",
        "node_ev_charge_cap_share"    => "node_ev_charge_cap_share"
    ),
    "gen" => Dict{String,String}(
        "gen"                 => "gen",
        "ren"                 => "ren",
        "con"                 => "con",
        "dis"                 => "dis",
        "nondis"              => "nondis",
        "gen_min_power"       => "gen_min_power",
        "gen_variable_cost"   => "gen_variable_cost",
        "gen_max_power"       => "gen_max_power",
        "gen_max_energy"      => "gen_max_energy",
        "gen_availability"    => "gen_availability",
        "gen_efficiency"      => "gen_efficiency",
        "gen_fuel_cost"       => "gen_fuel_cost",
        "gen_carbon_content"  => "gen_carbon_content",
        "gen_marginal_cost"   => "gen_marginal_cost",
        "gen_fixed_cost"      => "gen_fixed_cost",
        "gen_overnight_cost"  => "gen_overnight_cost",
        "gen_investment_cost" => "gen_investment_cost",
        "gen_lifetime"        => "gen_lifetime",
        "gen_tech_enabled"    => "gen_tech_enabled"
    ),
    "sto" => Dict{String,String}(
        "sto"                           => "sto",
        "inflow"                        => "inflow",
        "sto_min_power_out"             => "sto_min_power_out",
        "sto_min_energy"                => "sto_min_energy",
        "sto_max_energy"                => "sto_max_energy",
        "sto_investment_cost_energy"    => "sto_investment_cost_energy",
        "sto_efficiency_in"             => "sto_efficiency_in",
        "sto_efficiency_out"            => "sto_efficiency_out",
        "sto_marginal_cost"             => "sto_marginal_cost",
        "sto_fixed_cost_energy"         => "sto_fixed_cost_energy",
        "sto_fixed_cost_power_in"       => "sto_fixed_cost_power_in",
        "sto_fixed_cost_power_out"      => "sto_fixed_cost_power_out",
        "sto_investment_cost_power_out" => "sto_investment_cost_power_out",
        "sto_static_efficiency"         => "sto_static_efficiency",
        "sto_inflow"                    => "sto_inflow",
        "sto_overnight_cost_power_in"   => "sto_overnight_cost_power_in",
        "sto_min_power_in"              => "sto_min_power_in",
        "sto_max_power_out"             => "sto_max_power_out",
        "sto_investment_cost_power_in"  => "sto_investment_cost_power_in",
        "sto_lifetime"                  => "sto_lifetime",
        "sto_max_power_in"              => "sto_max_power_in",
        "sto_overnight_cost_power_out"  => "sto_overnight_cost_power_out",
        "sto_etop_max"                  => "sto_etop_max",
        "sto_overnight_cost_energy"     => "sto_overnight_cost_energy",
        "sto_tech_enabled"              => "sto_tech_enabled"
    ),
    "net" => Dict{String,String}(
        "l"                    => "l",
        "net_max_flow"         => "net_max_flow",
        "net_incidence_matrix" => "net_incidence_matrix",
        "net_efficiency"       => "net_efficiency"
    ),
    "ext" => Dict{String,String}(
        "extensions" => "extensions"
    ),
    "ev" => Dict{String,String}(
        "ev"                                  => "ev",
        "swap"                               => "swap",
        "ev_efficiency_discharge"             => "ev_efficiency_discharge",
        "ev_efficiency_charge"                => "ev_efficiency_charge",
        "ev_exogenous_enabled"                => "ev_exogenous_enabled",
        "ev_min_soc"                          => "ev_min_soc",
        "ev_discharge_enabled"                => "ev_discharge_enabled",
        "ev_bidirectional_time"               => "ev_bidirectional_time",
        "ev_marginal_cost_charge"             => "ev_marginal_cost_charge",
        "ev_share"                            => "ev_share",
        "ev_catenary_enabled"                 => "ev_catenary_enabled",
        "ev_station_power_rating"             => "ev_station_power_rating",
        "ev_driving_consumption"              => "ev_driving_consumption",
        "ev_battery_capacity"                 => "ev_battery_capacity",
        "ev_max_soc"                          => "ev_max_soc",
        "ev_grid_electricity_demand"          => "ev_grid_electricity_demand",
        "ev_marginal_cost_discharge"          => "ev_marginal_cost_discharge",
        "ev_profile_enabled"                  => "ev_profile_enabled",
        "ev_marginal_cost_direct"             => "ev_marginal_cost_direct",
        "ev_swap_enabled"                     => "ev_swap_enabled",
        "ev_swap_time"                        => "ev_swap_time",
        "ev_swap_station_enabled"             => "ev_swap_station_enabled",
        "ev_swap_overnight_cost_energy"       => "ev_swap_overnight_cost_energy",
        "ev_swap_overnight_cost_power"        => "ev_swap_overnight_cost_power",
        "ev_swap_lifetime"                    => "ev_swap_lifetime",
        "ev_swap_investment_cost_energy"      => "ev_swap_investment_cost_energy",
        "ev_swap_investment_cost_power"       => "ev_swap_investment_cost_power",
        "ev_swap_marginal_cost"               => "ev_swap_marginal_cost",
        "ev_swap_fixed_cost_energy"           => "ev_swap_fixed_cost_energy",
        "ev_swap_fixed_cost_power"            => "ev_swap_fixed_cost_power",
        "ev_swap_efficiency_charge"           => "ev_swap_efficiency_charge",
        "ev_swap_efficiency_static"           => "ev_swap_efficiency_static",
        "ev_swap_full_premium"                => "ev_swap_full_premium",
        "ev_charge_enabled"                   => "ev_charge_enabled"
    ),
    "h2" => Dict{String,String}(
        "stoh2"                            => "stoh2",
        "elyh2"                            => "elyh2",
        "reconh2"                          => "reconh2",
        "h2_availability_ely"              => "h2_availability_ely",
        "h2_max_energy_sto"                => "h2_max_energy_sto",
        "h2_lifetime_recon"                => "h2_lifetime_recon",
        "h2_lifetime_ely"                  => "h2_lifetime_ely",
        "h2_min_power_ely"                 => "h2_min_power_ely",
        "h2_reconversion_variable_cost"    => "h2_reconversion_variable_cost",
        "h2_max_power_ely"                 => "h2_max_power_ely",
        "h2_compression_electrical_demand" => "h2_compression_electrical_demand",
        "h2_overnight_cost_recon"          => "h2_overnight_cost_recon",
        "h2_overnight_cost_comp"           => "h2_overnight_cost_comp",
        "h2_investment_cost_sto"           => "h2_investment_cost_sto",
        "h2_min_power_recon"               => "h2_min_power_recon",
        "h2_fixed_cost_recon"              => "h2_fixed_cost_recon",
        "h2_overnight_cost_sto"            => "h2_overnight_cost_sto",
        "h2_investment_cost_comp"          => "h2_investment_cost_comp",
        "h2_lifetime_sto"                  => "h2_lifetime_sto",
        "h2_efficiency_comp"               => "h2_efficiency_comp",
        "h2_investment_cost_ely"           => "h2_investment_cost_ely",
        "h2_availability_sto"              => "h2_availability_sto",
        "h2_fixed_cost_sto"                => "h2_fixed_cost_sto",
        "h2_static_efficiency_sto"         => "h2_static_efficiency_sto",
        "h2_lifetime_comp"                 => "h2_lifetime_comp",
        "h2_level_min_sto"                 => "h2_level_min_sto",
        "h2_fixed_cost_comp"               => "h2_fixed_cost_comp",
        "h2_min_energy_sto"                => "h2_min_energy_sto",
        "h2_investment_cost_recon"         => "h2_investment_cost_recon",
        "h2_overnight_cost_ely"            => "h2_overnight_cost_ely",
        "h2_fixed_cost_ely"                => "h2_fixed_cost_ely",
        "h2_compression_variable_cost"     => "h2_compression_variable_cost",
        "h2_efficiency_ely"                => "h2_efficiency_ely",
        "h2_max_power_recon"               => "h2_max_power_recon",
        "h2_hydrogen_hr_demand"            => "h2_hydrogen_hr_demand",
        "h2_ely_enabled"                   => "h2_ely_enabled",
        "h2_recon_enabled"                 => "h2_recon_enabled",
        "h2_sto_enabled"                   => "h2_sto_enabled",
        "h2_efficiency_recon"              => "h2_efficiency_recon",
        "arcsh2"                           => "arcsh2",
        "connectorsh2"                     => "connectorsh2",
        "importsh2"                        => "importsh2",
        "h2_incidence_import"              => "h2_incidence_import",
        "h2_incidence_export"              => "h2_incidence_export",
        "h2_incidence"                     => "h2_incidence",
        "h2_max_flow"                      => "h2_max_flow",
        "h2_arc_efficiency"                => "h2_arc_efficiency",
        "h2_flow_comp_demand"              => "h2_flow_comp_demand",
        "h2_import_costs"                  => "h2_import_costs",
    ),
    "heat" => Dict{String,String}(
      "heat_sink"                          => "heat_sink",
      "heat_building"                      => "heat_building",
      "heat_tech"                          => "heat_tech",
      "heat_enabled"                       => "heat_enabled",
      "heat_dc_demand"                     => "heat_dc_demand",
      "heat_dc_cop"                        => "heat_dc_cop",
      "heat_share"                         => "heat_share",
      "heat_storage_efficiency"            => "heat_storage_efficiency",
      "heat_storage_static_efficiency"     => "heat_storage_static_efficiency",
      "heat_storage_duration"              => "heat_storage_duration"
    )
),
# MODULE > METADATA KEYS > HEADER NAME THAT CONTAINS MORE SYMBOLS (PARAMETERS)
"tables_map" => Dict{String, Dict{String,String}}(
    "scalars" => Dict{String,String}(    "scalars_table" => "parameters"),
    "node"    => Dict{String,String}(      "nodes_table" => "parameters"),
    "gen"     => Dict{String,String}(   "gen_tech_table" => "parameters"),
    "sto"     => Dict{String,String}(   "sto_tech_table" => "parameters"),
    "net"     => Dict{String,String}(      "lines_table" => "parameters"),
    "ev"      => Dict{String,String}(
      "ev_profiles_table" => "parameters",
      "ev_swap_stations_table" => "parameters",
      ),
    "h2"      => Dict{String,String}(
        "h2_recon_table" => "parameters",
          "h2_sto_table" => "parameters",
          "h2_ely_table" => "parameters",
          "h2_net_table" => "parameters", # this table should be in metadata.json
      ),
    "heat"    => Dict{String,String}(
        "heat_dc_demand" => "parameters",
        "heat_dc_cop"   => "parameters",
        "heat_technology"=> "parameters",
      ),
)
)
# This is used when no metadata.json file is provided. Make sure the csv files are located as file_name key indicates
metadata = Dict{String, Dict{String, Any}}(
  "sto_tech_table"=> Dict{String, Any}(
    "file_name"=> ["core","table_storage_tech.csv"],
    "set_col"=> "parameters",
    "set_row"=> ["n", "sto"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "sto"=> Dict{String, Any}(
    "file_name"=> ["core","sets","sto.csv"],
    "type"=> "set"
  ),
  "ev_station_power_rating"=> Dict{String, Any}(
    "file_name"=> ["ev","par_station_power_rating.csv"],
    "set_col"=> "ev",
    "set_row"=> ["n", "h"],
    "col_loc"=> 1,
    "type"=> "par"
  ),
  "lines_table"=> Dict{String, Any}(
    "file_name"=> ["core","table_lines.csv"],
    "set_col"=> "parameters",
    "set_row"=> ["l"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "ev_grid_electricity_demand"=> Dict{String, Any}(
    "file_name"=> ["ev","par_grid_electricity_demand.csv"],
    "set_col"=> "ev",
    "set_row"=> ["n", "h"],
    "col_loc"=> 1,
    "type"=> "par"
  ),
  "gen"=> Dict{String, Any}(
    "file_name"=> ["core","sets","gen.csv"],
    "type"=> "set"
  ),
  "h"=> Dict{String, Any}(
    "file_name"=> ["core","sets","h.csv"],
    "type"=> "set"
  ),
  "net_incidence_matrix"=> Dict{String, Any}(
    "file_name"=> ["core","par_incidence.csv"],
    "set_col"=> "n",
    "set_row"=> ["l"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "scalars_table"=> Dict{String, Any}(
    "file_name"=> ["core","table_scalars.csv"],
    "set_col"=> nothing,
    "set_row"=> ["parameters"],
    "col_loc"=> -1,
    "type"=> "par"
  ),
  "inflow"=> Dict{String, Any}(
    "file_name"=> ["core","sets","inflow.csv"],
    "type"=> "set"
  ),
  "nondis"=> Dict{String, Any}(
    "file_name"=> ["core","sets","nondis.csv"],
    "type"=> "set"
  ),
  "gen_tech_table"=> Dict{String, Any}(
    "file_name"=> ["core","table_generating_tech.csv"],
    "set_col"=> "parameters",
    "set_row"=> ["n", "gen"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "extensions"=> Dict{String, Any}(
    "file_name"=> ["extensions.csv"],
    "set_col"=> "ext",
    "set_row"=> ["n"],
    "col_loc"=> -1,
    "type"=> "par"
  ),
  "l"=> Dict{String, Any}(
    "file_name"=> ["core","sets","l.csv"],
    "type"=> "set"
  ),
  "con"=> Dict{String, Any}(
    "file_name"=> ["core","sets","cons.csv"],
    "type"=> "set"
  ),
  "nodes_table"=> Dict{String, Any}(
    "file_name"=> ["core","table_nodes.csv"],
    "set_col"=> "parameters",
    "set_row"=> ["n"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "dis"=> Dict{String, Any}(
    "file_name"=> ["core","sets","dis.csv"],
    "type"=> "set"
  ),
  "ev_profiles_table"=> Dict{String, Any}(
    "file_name"=> ["ev","table_ev_profiles.csv"],
    "set_col"=> "parameters",
    "set_row"=> ["n", "ev"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "ev_driving_consumption"=> Dict{String, Any}(
    "file_name"=> ["ev","par_driving_consumption.csv"],
    "set_col"=> "ev",
    "set_row"=> ["n", "h"],
    "col_loc"=> 1,
    "type"=> "par"
  ),
  "node_electricity_demand"=> Dict{String, Any}(
    "file_name"=> ["core","par_node_electricity_demand.csv"],
    "set_col"=> "n",
    "set_row"=> ["h"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "gen_availability"=> Dict{String, Any}(
    "file_name"=> ["core","par_availability.csv"],
    "set_col"=> "gen",
    "set_row"=> ["n", "h"],
    "col_loc"=> 1,
    "type"=> "par"
  ),
  "sto_inflow"=> Dict{String, Any}(
    "file_name"=> ["core","par_inflow.csv"],
    "set_col"=> "inflow",
    "set_row"=> ["n", "h"],
    "col_loc"=> 1,
    "type"=> "par"
  ),
  "ev"=> Dict{String, Any}(
    "file_name"=> ["ev","sets","ev.csv"],
    "type"=> "set"
  ),
  "ren"=> Dict{String, Any}(
    "file_name"=> ["core","sets","ren.csv"],
    "type"=> "set"
  ),
  "n"=> Dict{String, Any}(
    "file_name"=> ["core","sets","n.csv"],
    "type"=> "set"
  ),
  "ev_bidirectional_time"=> Dict{String, Any}(
    "file_name"=> ["ev","par_bidirectional_time.csv"],
    "set_col"=> "ev",
    "set_row"=> ["n", "h"],
    "col_loc"=> 1,
    "type"=> "par"
  ),
  "elyh2"=> Dict{String, Any}(
    "file_name"=> ["h2","sets","elyh2.csv"],
    "type"=> "set"
  ),
  "stoh2"=> Dict{String, Any}(
    "file_name"=> ["h2","sets","stoh2.csv"],
    "type"=> "set"
  ),
  "reconh2"=> Dict{String, Any}(
    "file_name"=> ["h2","sets","reconh2.csv"],
    "type"=> "set"
  ),
  "h2_hydrogen_hr_demand"=> Dict{String, Any}(
    "file_name"=> ["h2","par_h2_hydrogen_hr_demand.csv"],
    "set_col"=> "n",
    "set_row"=> ["h"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "h2_recon_table"=> Dict{String, Any}(
    "file_name"=> ["h2","table_recon_tech.csv"],
    "set_col"=> "parameters",
    "set_row"=> ["n", "reconh2"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "h2_sto_table"=> Dict{String, Any}(
    "file_name"=> ["h2","table_stoh2_tech.csv"],
    "set_col"=> "parameters",
    "set_row"=> ["n", "stoh2"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  "h2_ely_table"=> Dict{String, Any}(
    "file_name"=> ["h2","table_ely_tech.csv"],
    "set_col"=> "parameters",
    "set_row"=> ["n", "elyh2"],
    "col_loc"=> 0,
    "type"=> "par"
  ),
  # Update this when adding new technologies
)
