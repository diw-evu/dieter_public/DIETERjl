# Build DieterModel
function cond_tech(tech::AbstractString, node::AbstractString, dtr::DieterModel)
    if tech in dtr.maps[:gen][:base][node]
        return true
    else
        return false
    end
end

function cond_sto(sto::AbstractString, node::AbstractString, dtr::DieterModel)
    if sto in dtr.maps[:sto][:base][node]
        return true
    else
        return false
    end
end

function cond_net(line::AbstractString, dtr::DieterModel)
    test = []
    for node in dtr.sets[:n]
        if line in dtr.maps[:net][:base][node]
            push!(test, true)
        else
            push!(test, false)
        end
    end
    return sum(test) == 2
end

annuity(i,lifetime) = i*((1+i)^lifetime) / (((1+i)^lifetime)-1)

function calc_inv_gen!(dtr::DieterModel)
    T = dtr.sets[:gen]
    N = dtr.sets[:n]
    oc = dtr.parameters[:gen_overnight_cost]
    lt = dtr.parameters[:gen_lifetime]
    i = dtr.parameters[:node_interest]

    dtr.parameters[:gen_investment_cost] = round.(DenseAxisArray([cond_tech(t,n,dtr) ? oc[n,t]*annuity(i[n], lt[n,t]) : missing for n in N, t in T], N, T), digits=3)
end

function calc_inv_storages!(dtr::DieterModel)
    S = dtr.sets[:sto]
    N = dtr.sets[:n]
    oce = dtr.parameters[:sto_overnight_cost_energy]
    ocpi = dtr.parameters[:sto_overnight_cost_power_in]
    ocpo = dtr.parameters[:sto_overnight_cost_power_out]
    lt = dtr.parameters[:sto_lifetime]
    i = dtr.parameters[:node_interest]

    dtr.parameters[:sto_investment_cost_energy]    = round.(DenseAxisArray([cond_sto(s,n,dtr) ? oce[n,s] *annuity(i[n], lt[n,s]) : missing for n in N,  s in S], N, S), digits=3)
    dtr.parameters[:sto_investment_cost_power_in]  = round.(DenseAxisArray([cond_sto(s,n,dtr) ? ocpi[n,s]*annuity(i[n], lt[n,s]) : missing for n in N,  s in S], N, S), digits=3)
    dtr.parameters[:sto_investment_cost_power_out] = round.(DenseAxisArray([cond_sto(s,n,dtr) ? ocpo[n,s]*annuity(i[n], lt[n,s]) : missing for n in N,  s in S], N, S), digits=3)

    return nothing
end

function calc_mc!(dtr::DieterModel)
    T   = dtr.sets[:gen]
    N   = dtr.sets[:n]
    fc  = dtr.parameters[:gen_fuel_cost]
    eff = dtr.parameters[:gen_efficiency]
    vc  = dtr.parameters[:gen_variable_cost]
    cc  = dtr.parameters[:gen_carbon_content]
    co2 = dtr.parameters[:node_co2_price]

    dtr.parameters[:gen_marginal_cost] = round.(DenseAxisArray([cond_tech(t,n,dtr) ? fc[n,t]/eff[n,t] + (cc[n,t]*co2[n])/eff[n,t] + vc[n,t] : missing for n in N, t in T], N, T), digits=3)
end


function define_model!(m::JuMP.AbstractModel, dtr::DieterModel; maxhours::Int=8760, nthhour::Int=1, scen::Int=1, scale_dict::Dict{Symbol,Float64}=Dict([:energy,:currency,:co2,:obj] .=> [1.0 1.0 1.0 1.0]),apply_corr_factor::Bool=true)
    #------Getting maps and derived parameters
    update_map(dtr;feature=:gen, category=:base, maps=get_feature_map(dtr; symbol=:gen_tech_enabled, feature=:gen))
    update_map(dtr;feature=:sto, category=:base, maps=get_feature_map(dtr; symbol=:sto_tech_enabled, feature=:sto))
    update_map(dtr;feature=:net, category=:base, maps=get_feature_map(dtr; symbol=:net_incidence_matrix, feature=:net))
    calc_inv_gen!(dtr)
    calc_inv_storages!(dtr)
    calc_mc!(dtr)

    #--------------------------------------------------------------------------
    # Define meta data
    #--------------------------------------------------------------------------
    scenario_name = string("Run_", string(scen, pad=4), "_", Dates.format(now(),"yyyymmddHHMMSS"))
    dtr.settings[:name_scenario] = scenario_name
    dtr.settings[:maxhours] = maxhours
    dtr.settings[:nthhour] = nthhour
    dtr.settings[:scen_int] = scen


    hours_from_sets = dtr.sets[:h]
    index_hours = collect(1:nthhour:maxhours)
    corr_factor = apply_corr_factor ? length(index_hours)/8760 : 1.0
    length(hours_from_sets) < length(index_hours) && @warn "The number of hours from input data is less than the number of time steps selected"
    dtr.sets[:h] = hours_from_sets[index_hours]
    dtr.sets[:h_set] = hours_from_sets

    # unlim = 10e10
    
    #--------------------------------------------------------------------------
    # Define sets
    #--------------------------------------------------------------------------
    Hours = dtr.sets[:h]
    Nodes = dtr.sets[:n]
    # Generating technologies
    Technologies    = dtr.sets[:gen]
    Renewables      = dtr.sets[:ren]
    Conventional    = dtr.sets[:con]
    Dispatchable    = dtr.sets[:dis]
    NonDispatchable = dtr.sets[:nondis]
    # Storage technologies
    Storages = dtr.sets[:sto]
    Inflow = dtr.sets[:inflow]

    Lines = dtr.sets[:l]

    #--------------------------------------------------------------------------
    # Scaling factors
    for s in [:energy,:currency,:co2,:obj]
        if !haskey(dtr.scale_dict, s)
            dtr.scale_dict[s] = scale_dict[s]
        else
            if dtr.scale_dict[s] != scale_dict[s]
                if scale_dict[s] == 1.0
                    # keep original value from dtr.scale_dict
                    # we assume that 1.0 is the default value
                    pass()
                else
                    @warn "Overwriting scale factor for $(s) changed from $(dtr.scale_dict[s]) to $(scale_dict[s])"
                    dtr.scale_dict[s] = scale_dict[s]
                end
            end
        end
    end


    sf_ener = dtr.scale_dict[:energy]
    sf_curr = dtr.scale_dict[:currency]
    sf_co2 = dtr.scale_dict[:co2]
    obj_scale = dtr.scale_dict[:obj]


    #--------------------------------------------------------------------------
    # parameters
    #--------------------------------------------------------------------------

    infeas_cost              = dtr.parameters[:infeas_cost]*sf_curr # from scalars.csv
    # Nodes
    Load                     = dtr.parameters[:node_electricity_demand]*sf_ener
    CO2Cap                   = dtr.parameters[:node_co2_cap]*sf_co2 # is already in mn tCO2
    MinRES                   = dtr.parameters[:node_min_res_share]
    # Generating technologies
    InvestmentCost           = dtr.parameters[:gen_investment_cost]*sf_curr
    gen_marginal_cost        = dtr.parameters[:gen_marginal_cost]*sf_curr
    gen_fixed_cost           = dtr.parameters[:gen_fixed_cost]*sf_curr
    Availability             = dtr.parameters[:gen_availability]
    MaxInstallable           = dtr.parameters[:gen_max_power]*sf_ener
    MinInstallable           = dtr.parameters[:gen_min_power]*sf_ener
    MaxEnergyGen             = dtr.parameters[:gen_max_energy]*sf_ener
    Efficiency               = dtr.parameters[:gen_efficiency]
    CarbonContent            = dtr.parameters[:gen_carbon_content]
    # Storage technologies
    InvestmentCostPower_in   = dtr.parameters[:sto_investment_cost_power_in]*sf_curr
    InvestmentCostPower_out  = dtr.parameters[:sto_investment_cost_power_out]*sf_curr
    InvestmentCostEnergy     = dtr.parameters[:sto_investment_cost_energy]*sf_curr
    sto_marginal_cost        = dtr.parameters[:sto_marginal_cost]*sf_curr
    sto_fixed_cost_energy    = dtr.parameters[:sto_fixed_cost_energy]*sf_curr
    sto_fixed_cost_power_in  = dtr.parameters[:sto_fixed_cost_power_in]*sf_curr
    sto_fixed_cost_power_out = dtr.parameters[:sto_fixed_cost_power_out]*sf_curr
    sto_inflow               = dtr.parameters[:sto_inflow]*sf_ener
    MaxPower_in              = dtr.parameters[:sto_max_power_in]*sf_ener
    MaxPower_out             = dtr.parameters[:sto_max_power_out]*sf_ener
    MinEnergySto             = dtr.parameters[:sto_min_energy]*sf_ener
    MaxEnergySto             = dtr.parameters[:sto_max_energy]*sf_ener
    MinPower_in              = dtr.parameters[:sto_min_power_in]*sf_ener
    MinPower_out             = dtr.parameters[:sto_min_power_out]*sf_ener
    Efficiency_in            = dtr.parameters[:sto_efficiency_in]
    Efficiency_out           = dtr.parameters[:sto_efficiency_out]
    StaticEfficiency         = dtr.parameters[:sto_static_efficiency]
    EtopMax                  = dtr.parameters[:sto_etop_max]
    # Network
    MaxFlow                  = dtr.parameters[:net_max_flow]*sf_ener
    # FlowEfficiency          = dtr.parameters[:net_efficiency]
    IncidenceMatrix          = container_to_dense(dtr.parameters[:net_incidence_matrix]; dim_names=get_dimenssions(:net_incidence_matrix), sets_map=dtr.sets)

    prog = Progress(10, 0.01, "Building Model...         ", 40)



    next!(prog)
    #--------------------------------------------------------------------------
    # Variables
    #--------------------------------------------------------------------------

    # Variables core module
    @variable(m, G[n=Nodes, t=Technologies, h=Hours; cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0] >= 0) # Generation
    @variable(m, CU[n=Nodes, t=NonDispatchable, h=Hours; cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0] >= 0) # Curtailment
    @variable(m, STO_IN[n=Nodes, sto=Storages, h=Hours; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0] >= 0) # Storage charge
    @variable(m, STO_OUT[n=Nodes, sto=Storages, h=Hours; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0] >= 0) # Storage discharge
    @variable(m, STO_L[n=Nodes, sto=Storages, h=Hours; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0] >= 0) # Storage level
    @variable(m, STO_SPILL[n=Nodes, sto=Inflow, h=Hours; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0] >= 0) # Spill from storage
    @variable(m, N_TECH[n=Nodes, t=Technologies; cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0] >= 0) # Investment in generation capacities
    @variable(m, N_STO_E[n=Nodes, sto=Storages; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0] >= 0) # Investment in storage energy energy capacity
    @variable(m, N_STO_P_IN[n=Nodes, sto=Storages; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0] >= 0) # Investment in storage charging capacity
    @variable(m, N_STO_P_OUT[n=Nodes, sto=Storages; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0] >= 0)  # Investment in storage discharging capacity
    @variable(m, G_INF[n=Nodes,h=Hours] >= 0) # Loss of load
    @variable(m, F[h=Hours,l=Lines; cond_net(l, dtr)]) # Interconnection flow on line l in hour h


    next!(prog)
    #--------------------------------------------------------------------------
    # Objective function
    #--------------------------------------------------------------------------
    # Define core objective expression
    obj_expr = @expression(m,
    (1/corr_factor)*(sum(gen_marginal_cost[n,t] * G[n,t,h] for n in Nodes, t in Technologies, h in Hours if cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0)

    + sum(infeas_cost * G_INF[n,h] for n in Nodes, h in Hours)

    + sum(sto_marginal_cost[n,sto] * (STO_OUT[n,sto,h] + STO_IN[n,sto,h]) for n in Nodes, sto in Storages, h in Hours if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
    )
    + 
        ( sum(InvestmentCost[n,t] * N_TECH[n,t] for n in Nodes, t in Technologies if cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0)
        + sum(InvestmentCostPower_in[n,sto] * N_STO_P_IN[n,sto] for n in Nodes, sto in Storages if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
        + sum(InvestmentCostPower_out[n,sto] * N_STO_P_OUT[n,sto] for n in Nodes, sto in Storages if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
        + sum(InvestmentCostEnergy[n,sto] * N_STO_E[n,sto] for n in Nodes, sto in Storages if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
        + sum(gen_fixed_cost[n,t] * N_TECH[n,t] for n in Nodes, t in Technologies if cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0)
        + sum(sto_fixed_cost_energy[n,sto] * N_STO_E[n,sto] for n in Nodes, sto in Storages if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
        + sum(sto_fixed_cost_power_in[n,sto] * N_STO_P_IN[n,sto] for n in Nodes, sto in Storages if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
        + sum(sto_fixed_cost_power_out[n,sto] * N_STO_P_OUT[n,sto] for n in Nodes, sto in Storages if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
        )
        );

    next!(prog)
    
    #--------------------------------------------------------------------------
    # Constraints - Energy balance
    #--------------------------------------------------------------------------

    @expression(m,ebal[n=Nodes,h=Hours],
                # Supply side
            sum(G[n,t,h] for t in Technologies if cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0)
            + sum(STO_OUT[n,sto,h] for sto in Storages if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
            + G_INF[n,h]
                # Demand side
            - sum(IncidenceMatrix[n,l]*F[h,l] for l in Lines if cond_net(l, dtr) && IncidenceMatrix[n,l] != 0)
            - sum(STO_IN[n,sto,h] for sto in Storages if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
            - Load[n,h]  
    );

    next!(prog)

    #--------------------------------------------------------------------------
    # Constraints - Core module
    #--------------------------------------------------------------------------

    @constraint(m, MaxGenerationDisp[n=Nodes,t=Dispatchable,h=Hours; cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0],
        G[n,t,h] <= N_TECH[n,t]
        ); # Generation limits Dispatchable technologies

    @constraint(m, MaxGenerationNondisp[n=Nodes,t=NonDispatchable,h=Hours; cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0],
        G[n,t,h] + CU[n,t,h] == Availability[n,t,h] * N_TECH[n,t]
        ); # Generation of non-NonDispatchable technologies 

    for n=Nodes,t=Technologies
        if cond_tech(t,n, dtr) && MaxInstallable[n,t] > 0
            set_upper_bound(N_TECH[n,t], MaxInstallable[n,t])
        end
        if cond_tech(t,n,dtr) && MaxInstallable[n,t] !=0
            set_lower_bound(N_TECH[n,t], MinInstallable[n,t])
        end
    end

    fac = length(Hours)/8760
    @constraint(m, MaxEnergyGenerated[n=Nodes,t=Technologies; cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0 && MaxEnergyGen[n,t] >= 0],  # -1 means infinite, if 0, the variable was not created
        sum(G[n,t,h] for h in Hours) <= fac * MaxEnergyGen[n,t]
        ); # Maximum generation constraints (biomass)

    next!(prog)

    @constraint(m, MaxWithdrawStorage[n=Nodes,sto=Storages,h=Hours; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0],
        STO_IN[n,sto,h] <= N_STO_P_IN[n,sto]
        ); # Maximum storage carge

    @constraint(m, MaxGenerationStorage[n=Nodes,sto=Storages,h=Hours; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0],
        STO_OUT[n,sto,h] <= N_STO_P_OUT[n,sto]
        ); # Maximum storage discharge

    @constraint(m, MaxLevelStorage[n=Nodes,sto=Storages,h=Hours; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0],
        STO_L[n,sto,h] <= N_STO_E[n,sto]
        ); # Maximum storage level


    for n=Nodes, sto=Storages
        if cond_sto(sto,n, dtr)
            if MaxPower_out[n,sto] != 0
                if MaxEnergySto[n,sto] >= 0
                    set_upper_bound(N_STO_E[n,sto], MaxEnergySto[n,sto])
                end
                if MaxEnergySto[n,sto] != 0 && MinEnergySto[n,sto] > 0
                    set_lower_bound(N_STO_E[n,sto], MinEnergySto[n,sto])
                end
                if MaxPower_in[n,sto] >= 0
                    set_upper_bound(N_STO_P_IN[n,sto], MaxPower_in[n,sto])
                end
                if MinPower_in[n,sto] > 0
                    set_lower_bound(N_STO_P_IN[n,sto], MinPower_in[n,sto])
                end
                if MaxPower_out[n,sto] > 0
                    set_upper_bound(N_STO_P_OUT[n,sto], MaxPower_out[n,sto])
                end
                if MinPower_out[n,sto] > 0
                    set_lower_bound(N_STO_P_OUT[n,sto], MinPower_out[n,sto])
                end
            end
        end
    end
            

    @constraint(m, StorageBalance[n=Nodes, sto=Storages, h=Hours; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0],
        STO_L[n, sto, h]
        ==
        StaticEfficiency[n,sto]*STO_L[n, sto, preceding_period(Hours,h)]
        + STO_IN[n, sto, h]*Efficiency_in[n, sto]
        + sum(sto_inflow[n,ifw,h] for ifw in Inflow if ifw == sto; init=0)  # In fact there is no summation as only one element can be selected, if no element, then 0.
        - STO_OUT[n, sto, h]/Efficiency_out[n, sto]
        - sum(STO_SPILL[n,ifw,h] for ifw in Inflow if ifw == sto; init=0) 
        ); # Storage plant equation

    @constraint(m, EtopConstraint[n=Nodes,sto=Storages; cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0],
        N_STO_E[n,sto] <= EtopMax[n,sto]*N_STO_P_OUT[n,sto] 
        );

    
    for l=Lines, h=Hours
        if cond_net(l, dtr)
            if MaxFlow[l] >= 0
                set_upper_bound(F[h,l], MaxFlow[l])
                set_lower_bound(F[h,l], -MaxFlow[l])
            end
        end
    end


    @constraint(m,CO2Constraint[n=Nodes; CO2Cap[n] >= 0],
        sum(G[n,t,h]*CarbonContent[n,t]/Efficiency[n,t]/(1e6*sf_ener) 
        for  h in Hours, t in Technologies if cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0; init=0) 
            <= CO2Cap[n]
        );

    @constraint(m, G_INF_Disabled[n=Nodes,h=Hours; infeas_cost==-1],
            G_INF[n,h] == 0
        );

    # Generate expression for base minimum Renewables constraint To be extended in other modules and 
    # added as a constraint below
    @expression(m,min_res_expr[n=Nodes; MinRES[n] > 0],
        MinRES[n]*(sum(Load[n,h] for h in Hours))
        # Usually losses are covered 100% by RES to avoid USC
        + sum(STO_IN[n,sto,h] + sum(sto_inflow[n,ifw,h] for ifw in Inflow if ifw == sto; init=0) - STO_OUT[n,sto,h] for sto in Storages, h in Hours if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0)
        # Generation side below
        - sum(G[n,t,h] for t in Renewables, h in Hours if cond_tech(t,n, dtr) && MaxInstallable[n,t] != 0; init=0)
        - sum(sum(sto_inflow[n,ifw,h] for ifw in Inflow if ifw == sto; init=0) for sto in Storages, h in Hours if cond_sto(sto,n, dtr) && MaxPower_out[n,sto] != 0; init=0)
    );

    next!(prog)


    #--------------------------------------------------------------------------
    # Add modules (order matters as some modules affect others)
    #--------------------------------------------------------------------------
    if cond_ext(:heat, "all", dtr) heat_module(m, dtr, obj_expr, ebal, min_res_expr, corr_factor,scale_dict) end
    next!(prog)
    #if cond_ext(:heat, "all", dtr) && cond_ext(:dh,"all",dtr) district_heat_module(m, dtr, obj_expr, ebal, min_res_expr, corr_factor,scale_dict) end
    if cond_ext(:ev, "all", dtr) ev_module(m, dtr, obj_expr, ebal, min_res_expr, corr_factor,scale_dict) end
    next!(prog)
    if cond_ext(:h2, "all",dtr) h2_module(m, dtr, obj_expr, ebal, min_res_expr, corr_factor,scale_dict) end
    next!(prog)
    

    #--------------------------------------------------------------------------
    # Compile complicating constraints and objective function
    #--------------------------------------------------------------------------
    if length(Hours) > 0 
        # compile energy balance
        @constraint(m,EnergyBalance[n=Nodes,h=Hours], ebal[n,h]==0);
    
        #compile minimum renewables constraint
        @constraint(m,MinRESConstraint[n=Nodes; MinRES[n] > 0], min_res_expr[n] >= 0);
    end
        
    # compile objective function
    @objective(m, Min,obj_scale*obj_expr);

    next!(prog)

    return nothing
end
