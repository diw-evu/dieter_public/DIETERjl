
mutable struct DieterModel
    maps::Dict{Symbol,Dict{Symbol,Dict{String,Vector{String}}}}
    ext::Dict{Symbol,Dict{String,Int64}}
    sets::Dict{Symbol, Array{String,1}}
    parameters::Dict{Symbol, Any}
    settings::Dict{Symbol, Any}
    results::Dict{Symbol, Dict{Symbol, Any}}
    symbols::Dict{Symbol, Any}
    modules::Dict{Symbol,Vector{Symbol}}
    mapping::Dict{String,Dict{String,Dict{String,String}}}
    metadata::Dict{String,Any}
    sets_map::Dict{Symbol,Symbol}
    scale_dict::Dict{Symbol,Float64}
    

    function DieterModel(first_arg::Union{Dict{Symbol,DataFrame}, AbstractString}; dense_or_sparse::Symbol=:dense, verbose=false)
        """
        This function sets up a DieterModel parsing input files into model inputs.
        """
        if first_arg isa AbstractString
            path = first_arg
            dataframe_dict = nothing
        elseif first_arg isa Dict{Symbol,DataFrame}
            dataframe_dict = first_arg
            path = ""
        end
        
        self                      = new()
        self.maps                 = Dict{Symbol,Dict{Symbol,Dict{String,Vector{String}}}}()
        self.ext                  = Dict{Symbol,Dict{String,Int64}}()
        self.settings             = Dict{Symbol,Any}()
        self.sets                 = Dict{Symbol,Array{String,1}}()
        self.parameters           = Dict{Symbol,Any}()
        self.results              = Dict{Symbol,Any}()
        self.modules              = Dict{Symbol,Vector{Symbol}}()
        self.symbols              = Dict{Symbol,Dict{Symbol,String}}()
        self.mapping              = Dict{String,Dict{String,Dict{String,String}}}()
        self.metadata             = Dict{String,Any}()
        self.sets_map             = Dict{Symbol,Symbol}()
        self.scale_dict           = Dict{Symbol,Float64}()


        self.modules[:core]       = core_modules
        self.modules[:add_ons]    = extension_modules
        self.symbols              = symbols

        if verbose
            println("Loading data...")
        end

        if dataframe_dict === nothing
            metadata_file = joinpath(path,"metadata.json")
            if isfile(metadata_file)
                if verbose
                    println("Loading metadata from file...")
                end
                self.metadata = JSON.parsefile(metadata_file)
            else
                if verbose
                    println("Loading metadata from template...")
                end
                self.metadata = metadata
            end
            
            mapping_file  = joinpath(path,"maps.json")
            if isfile(mapping_file)
                if verbose
                    println("Loading mapping from file...")
                end
                self.mapping = JSON.parsefile(mapping_file)
            else
                if verbose
                    println("Loading mapping from template...")
                end
                self.mapping = mapping
            end
            # Add special ext symbol
            self.sets[:ext] = string.(self.modules[:add_ons])
            self.sets_map[:ext] = :ext
            # loop through modules and symbols
            for feature in self.modules[:core]
                collect_sets(self, path, feature, verbose)
            end
            for feature in self.modules[:core]
                collect_data(self, path, feature, dense_or_sparse, verbose)
            end
        else
            no_missing_symbols, missing_symbs_dict = check_all_symbols_in_dataframe_dict(self, dataframe_dict, features=self.modules[:core])
            @assert no_missing_symbols "Not all symbols in input data were found in input file."
            for feature in self.modules[:core]
                get_sets_from_dataframe_dict(self, dataframe_dict, feature=feature, verbose=verbose)
            end
            for feature in self.modules[:core]
                get_parameters_from_dataframe_dict(self, dataframe_dict, feature=feature, dense_or_sparse=dense_or_sparse, verbose=verbose)
            end
        end
        if verbose
            println("Finished loading core features data")
        end


        # Extentions ------------------------------------
        build_ext_node_dict(self)

        # Electric vehicles
        if sum(self.ext[:ev][n] for n in self.sets[:n]) > 0
            if verbose
                println("Loading EV data...")
            end
            if dataframe_dict === nothing
                collect_sets(self, path, :ev, verbose)
                collect_data(self, path, :ev, dense_or_sparse, verbose)
            else
                no_missing_symbols, missing_symbs_dict = check_all_symbols_in_dataframe_dict(self, dataframe_dict, features=[:ev])
                @assert no_missing_symbols "Not all symbols in input data were found in input file."
                get_sets_from_dataframe_dict(self, dataframe_dict, feature=:ev, verbose=verbose)
                get_parameters_from_dataframe_dict(self, dataframe_dict, feature=:ev, dense_or_sparse=dense_or_sparse, verbose=verbose)
            end
        end

        # Hydrogen
        if sum(self.ext[:h2][n] for n in self.sets[:n]) > 0
            if verbose
                println("Loading H2 data...")
            end
            if dataframe_dict === nothing
                collect_sets(self, path, :h2, verbose)
                collect_data(self, path, :h2, dense_or_sparse, verbose)
            else
                no_missing_symbols, missing_symbs_dict = check_all_symbols_in_dataframe_dict(self, dataframe_dict, features=[:h2])
                @assert no_missing_symbols "Not all symbols in input data were found in input file."
                get_sets_from_dataframe_dict(self, dataframe_dict, feature=:h2, verbose=verbose)
                get_parameters_from_dataframe_dict(self, dataframe_dict, feature=:h2, dense_or_sparse=dense_or_sparse, verbose=verbose)
            end
        end

        # Heating
        if sum(self.ext[:heat][n] for n in self.sets[:n]) > 0
            if verbose
                println("Loading heat data...")
            end
            if dataframe_dict === nothing
                collect_sets(self, path, :heat, verbose)
                collect_data(self, path, :heat, dense_or_sparse, verbose)
            else
                no_missing_symbols, missing_symbs_dict = check_all_symbols_in_dataframe_dict(self, dataframe_dict, features=[:heat])
                @assert no_missing_symbols "Not all symbols in input data were found in input file."
                get_sets_from_dataframe_dict(self, dataframe_dict, feature=:heat, verbose=verbose)
                get_parameters_from_dataframe_dict(self, dataframe_dict, feature=:heat, dense_or_sparse=dense_or_sparse, verbose=verbose)
            end
        end
        if verbose
            println("Data load completed...")
        end

        return self
    end

    DieterModel() = new()
end
