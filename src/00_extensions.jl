# conditions for model
function cond_ext(ext::Symbol, node::AbstractString, dtr::DieterModel)
    if node == "all"
        if sum(values(dtr.ext[ext])) > 0
            return true
        else
            return false
        end
    else
        if dtr.ext[ext][node] > 0
            return true
        else
            return false
        end

    end
end

function build_ext_node_dict(dtr::DieterModel)
    add_ons = convert_jump_container_to_dict(dtr.parameters[:extensions])
    # Complete the dtr.ext with zero by default
    for ext in dtr.modules[:add_ons]
        for n in dtr.sets[:n]
            if haskey(add_ons, (n,string(ext),))
                if !haskey(dtr.ext, ext)
                    dtr.ext[ext] = Dict{String,Int64}()
                end
                if add_ons[(n,string(ext),)] === missing
                    dtr.ext[ext][n] = 0
                else
                    dtr.ext[ext][n] = add_ons[(n,string(ext),)]
                end
            else
                if !haskey(dtr.ext, ext)
                    dtr.ext[ext] = Dict{String,Int64}()
                end
                dtr.ext[ext][n] = 0
            end
        end
    end
end