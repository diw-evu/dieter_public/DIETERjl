module DIETER

using InteractiveUtils
using DataFrames
using CSV
using Missings
using JuMP
using JuMP.Containers
using MathOptInterface
using ProgressMeter
using Arrow
using JSON
using Dates
using Pipe
using InlineStrings

include("02_constant.jl")
include("01_struct.jl")
include("02_util.jl")
include("03_base.jl")
include("04_model.jl")
include("00_extensions.jl")
include("05_heat.jl")
include("06_results.jl")
include("07_ev.jl")
include("08_h2.jl")
include("heat_new.jl") # Preliminary

export DieterModel, 
        define_model!, 
        collect_results, 
        collect_symbols,
        collect_data,
        collect_sets,
        convert_dict_to_df,
        convert_long_df_to_dict, 
        convert_jump_container_to_df, 
        convert_jump_container_to_dict,
        convert_long_df_to_dense,
        convert_long_df_to_sparse,
        find_infeasible_constraints,
        get_variable_names,
        get_variables,
        get_dims,
        save_scenario,
        df_stack,
        annuity,
        sametype,
        sameas,
        symbols,
        iteration_table,
        prepare_scenario,
        scenario_optimization,
        derived_parameters,
        set_model_instance

end # module
