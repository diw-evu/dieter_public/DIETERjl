
function cond_ev(ev_tech::AbstractString, node::AbstractString, dtr::DieterModel)
    if ev_tech in dtr.maps[:ev][:base][node]
        return true
    else
        return false
    end
end

function cond_swap(ev_swap::AbstractString, node::AbstractString, dtr::DieterModel)
    if ev_swap in dtr.maps[:ev][:swap][node]
        return true
    else
        return false
    end
end

function sanity_check_ev(dtr::DieterModel)
    for (node, binary) in dtr.ext[:ev]
        if binary == 1
            addition = round(sum(dtr.parameters[:ev_share][node,ev] for ev in dtr.sets[:ev] if cond_ev(ev,node,dtr)), digits=3)
            @assert addition == 1 "Sum of ev_share must be 1 for every node. Node: $node, Sum: $addition"
        end
    end
end

function calc_inv_swap_station!(dtr::DieterModel)
    S = dtr.sets[:swap]
    N = dtr.sets[:n]
    oce = dtr.parameters[:ev_swap_overnight_cost_energy]
    ocpi = dtr.parameters[:ev_swap_overnight_cost_power]
    lt = dtr.parameters[:ev_swap_lifetime]
    i = dtr.parameters[:node_interest]

    dtr.parameters[:ev_swap_investment_cost_energy] = DenseAxisArray([cond_swap(s,n,dtr) ? oce[n,s] *annuity(i[n], lt[n,s]) : missing for n in N,  s in S], N, S)
    dtr.parameters[:ev_swap_investment_cost_power]  = DenseAxisArray([cond_swap(s,n,dtr) ? ocpi[n,s]*annuity(i[n], lt[n,s]) : missing for n in N,  s in S], N, S)

    return nothing
end

function find_index(vec, val)
    idx = findfirst(x -> x == val, vec)
    if isnothing(idx)
        error("Element $val not found")
    else
        return idx
    end
end


function ev_module(m::JuMP.AbstractModel,dtr::DieterModel, obj_expr, ebal, min_res_expr, corr_factor, scale_dict)
"""
Add Electric Vehicle Module
Description: Defines sets, variables, constraints and modifies existing constraints, e.g. the energy balance,
and the objective function. This function can only be called from inside the define_model function
"""

    update_map(dtr;feature=:ev, category=:base, maps=get_feature_map(dtr; symbol=:ev_profile_enabled, feature=:ev))
    update_map(dtr;feature=:ev, category=:swap, maps=get_feature_map(dtr; symbol=:ev_swap_station_enabled, feature=:ev))
    calc_inv_swap_station!(dtr)
    
    # Scaling factors
    sf_ener = !isnothing(scale_dict) ? scale_dict[:energy] : 1.0
    sf_curr = !isnothing(scale_dict) ? scale_dict[:currency] : 1.0

    # Define module-specific sets
    Nodes = dtr.sets[:n]
    Hours = dtr.sets[:h]
    evTech = dtr.sets[:ev]
    swapTech = dtr.sets[:swap]
    Technologies = dtr.sets[:gen]

    infeas_ev_cost           = dtr.parameters[:infeas_ev_cost]
    # Define Min RES parameters
    MinRES = dtr.parameters[:node_min_res_share]

    # Core module parameters
    MaxInstallable = dtr.parameters[:gen_max_power]*sf_ener

    # Extract module-specific parameters
    ev_quant                = dtr.parameters[:node_ev_quantity]
    ev_discharge_cap_share  = dtr.parameters[:node_ev_discharge_cap_share]
    ev_charge_cap_share     = dtr.parameters[:node_ev_charge_cap_share]

    # ev profile dependent
    ev_share                    = container_to_dense(dtr.parameters[:ev_share]; dim_names=get_dimenssions(:ev_share), sets_map=dtr.sets)
    ev_capacity                 = dtr.parameters[:ev_battery_capacity]*sf_ener
    ev_mc_charge                = dtr.parameters[:ev_marginal_cost_charge]*sf_curr
    ev_mc_discharge             = dtr.parameters[:ev_marginal_cost_discharge]*sf_curr
    ev_mc_direct                = dtr.parameters[:ev_marginal_cost_direct]*sf_curr
    ev_efficiency_charge        = dtr.parameters[:ev_efficiency_charge]
    ev_efficiency_discharge     = dtr.parameters[:ev_efficiency_discharge]
    ev_min_soc                  = container_to_dense(dtr.parameters[:ev_min_soc]; dim_names=get_dimenssions(:ev_min_soc), sets_map=dtr.sets)
    ev_max_soc                  = container_to_dense(dtr.parameters[:ev_max_soc]; dim_names=get_dimenssions(:ev_max_soc), sets_map=dtr.sets)
    # switches
    ev_catenary                 = container_to_dense(dtr.parameters[:ev_catenary_enabled]; dim_names=get_dimenssions(:ev_catenary_enabled), sets_map=dtr.sets)
    ev_exog                     = container_to_dense(dtr.parameters[:ev_exogenous_enabled]; dim_names=get_dimenssions(:ev_exogenous_enabled), sets_map=dtr.sets)
    ev_discharge                = container_to_dense(dtr.parameters[:ev_discharge_enabled]; dim_names=get_dimenssions(:ev_discharge_enabled), sets_map=dtr.sets)
    ev_swap                     = container_to_dense(dtr.parameters[:ev_swap_enabled]; dim_names=get_dimenssions(:ev_swap_enabled), sets_map=dtr.sets)
    ev_charges                   = container_to_dense(dtr.parameters[:ev_charge_enabled]; dim_names=get_dimenssions(:ev_charge_enabled), sets_map=dtr.sets)
    # Time-series
    ev_ed                       = dtr.parameters[:ev_driving_consumption]*sf_ener
    ev_ged_exog                 = dtr.parameters[:ev_grid_electricity_demand]*sf_ener
    ev_p                        = dtr.parameters[:ev_station_power_rating]*sf_ener
    ev_bi                       = dtr.parameters[:ev_bidirectional_time]
    ev_sw                       = dtr.parameters[:ev_swap_time]

    swap_investment_cost_energy = dtr.parameters[:ev_swap_investment_cost_energy]*sf_curr
    swap_investment_cost_power  = dtr.parameters[:ev_swap_investment_cost_power]*sf_curr
    swap_marginal_cost          = dtr.parameters[:ev_swap_marginal_cost]*sf_curr
    swap_fixed_cost_energy      = dtr.parameters[:ev_swap_fixed_cost_energy]*sf_curr
    swap_fixed_cost_power       = dtr.parameters[:ev_swap_fixed_cost_power]*sf_curr
    swap_efficiency_charge      = dtr.parameters[:ev_swap_efficiency_charge]
    swap_efficiency_static      = dtr.parameters[:ev_swap_efficiency_static]
    swap_full_premium           = dtr.parameters[:ev_swap_full_premium]*sf_curr


    sanity_check_ev(dtr)

    # Define module-specific variables
    @variable(m, EV_CHARGE[n=Nodes,ev=evTech,h=Hours;        cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0] >= 0)                              # Electric vehicle charging vehicle profile ev hour h [MWh]
    @variable(m, EV_DISCHARGE[n=Nodes,ev=evTech,h=Hours;     cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_discharge[n,ev]==1] >= 0)     # Electric vehicle discharging vehicle profile ev hour h [MWh]
    @variable(m, EV_L[n=Nodes,ev=evTech,h=Hours;             cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0] >= 0)                              # Electric vehicle charging level vehicle profile ev hour h [MWh]
    @variable(m, EV_GED[n=Nodes,ev=evTech,h=Hours;           cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0] >= 0)                              # Grid electricity demand for mobility vehicle profile ev hour h [MWh]
    @variable(m, EV_DIRECT[n=Nodes,ev=evTech,h=Hours;        cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_catenary[n,ev]==1] >= 0)      # Direct grid electricity demand for mobility vehicle profile ev hour h [MWh]
    @variable(m, EV_ED[n=Nodes,ev=evTech,h=Hours;            cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0] >= 0)                              # Electric vehicle energy demand vehicle profile ev hour h [MWh] depends on ev_ed for Smart, and on ev_ged_exog and ev_efficiency_charge for exog
    @variable(m, EV_INF[n=Nodes,ev=evTech,h=Hours;           cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0] >= 0)
    
    @variable(m, EV_SWAP_TIME[n=Nodes, ev=evTech, swap=swapTech, h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && cond_swap(swap,n,dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1] >= 0) # Electric vehicle battery swap vehicle profile ev hour h [MWh]
    @variable(m, EV_SWAP_N_E[n=Nodes, swap=swapTech;            cond_ext(:ev,n,dtr) && cond_swap(swap,n,dtr) && any(cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1 for ev in evTech)] >= 0)         # swap station storage energy capacity [MWh]
    @variable(m, EV_SWAP_N_P[n=Nodes, swap=swapTech;            cond_ext(:ev,n,dtr) && cond_swap(swap,n,dtr) && any(cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1 for ev in evTech)] >= 0)         # swap station storage power capacity [MW]
    @variable(m, EV_SWAP_IN[n=Nodes, swap=swapTech, h=Hours;    cond_ext(:ev,n,dtr) && cond_swap(swap,n,dtr) && any(cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1 for ev in evTech)] >= 0)         # swap station charging from the grid [MWh]
    @variable(m, EV_SWAP_L[n=Nodes, swap=swapTech, h=Hours;     cond_ext(:ev,n,dtr) && cond_swap(swap,n,dtr) && any(cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1 for ev in evTech)] >= 0)         # swap station battery energy level [MWh]
   

    adj_factor = corr_factor>0 ? 1/corr_factor : 0
    rev_hours = reverse(Hours)
    # Modify objective function
    add_to_expression!(obj_expr,
    adj_factor*( 
        sum(ev_mc_charge[n,ev]*EV_CHARGE[n,ev,h] for n in Nodes, ev in evTech, h in Hours if cond_ext(:ev,n,dtr) && cond_ev(ev,n, dtr) && ev_share[n,ev] > 0; init=0)
        + sum(ev_mc_discharge[n,ev]*EV_DISCHARGE[n,ev,h] for n in Nodes, ev in evTech, h in Hours if cond_ext(:ev,n,dtr) && cond_ev(ev,n, dtr) && ev_share[n,ev] > 0 && ev_discharge[n,ev]==1; init=0)
        + sum(ev_mc_direct[n,ev]*EV_DIRECT[n,ev,h] for n in Nodes, ev in evTech, h in Hours if cond_ext(:ev,n,dtr) && cond_ev(ev,n, dtr) && ev_share[n,ev] > 0 && ev_catenary[n,ev]==1; init=0)
        + sum(swap_marginal_cost[n,swap]*EV_SWAP_IN[n,swap,h] for n in Nodes, swap in swapTech, h in Hours if cond_ext(:ev,n,dtr) && cond_swap(swap,n, dtr) && any(cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1 for ev in evTech); init=0)
        - sum(swap_full_premium[n,swap]*EV_SWAP_TIME[n,ev,swap,h]/(ev_capacity[n,ev]*ev_share[n,ev]*ev_quant[n])*find_index(rev_hours, h) for n in Nodes, ev in evTech, swap in swapTech, h in Hours if cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && cond_swap(swap,n, dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1; init=0) # Idea is to prefer swapping more energy and earlier than later. Premium should be small. 
    )
    + sum(infeas_ev_cost * EV_INF[n,ev,h] for n in Nodes, ev in evTech, h in Hours if cond_ext(:ev,n,dtr) && cond_ev(ev,n, dtr) && ev_share[n,ev] > 0; init=0)
    + sum((swap_investment_cost_energy[n,swap] + swap_fixed_cost_energy[n,swap]) * EV_SWAP_N_E[n,swap] for n in Nodes, swap in swapTech if cond_ext(:ev,n,dtr) && cond_swap(swap,n, dtr) && any(cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1 for ev in evTech); init=0)
    + sum((swap_investment_cost_power[n,swap] + swap_fixed_cost_power[n,swap]) * EV_SWAP_N_P[n,swap] for n in Nodes, swap in swapTech if cond_ext(:ev,n,dtr) && cond_swap(swap,n, dtr) && any(cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1 for ev in evTech); init=0)
    );

    # Modify energy balance
    for n in Nodes
        for h in Hours
            add_to_expression!(ebal[n,h],
        # supply side
        + sum(EV_DISCHARGE[n,ev,h] for ev in evTech if cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_discharge[n,ev]==1; init=0)
        # Demand side
        - sum(EV_CHARGE[n,ev,h] for ev in evTech if cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0; init=0)
        - sum(EV_DIRECT[n,ev,h] for ev in evTech if cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_catenary[n,ev]==1; init=0)
        - sum(EV_SWAP_IN[n,swap,h] for swap in swapTech if cond_ext(:ev,n,dtr) && cond_swap(swap,n, dtr) && any(cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_swap[n,ev]==1 for ev in evTech); init=0)
        );
    end
        if MinRES[n]>0 && cond_ext(:ev,n,dtr)
            add_to_expression!(min_res_expr[n],
                sum(
                    MinRES[n] * sum(
                        + EV_GED[n,ev,hh]/ev_efficiency_charge[n,ev]
                        + sum(EV_DIRECT[n,evev,hh] for evev in [ev] if ev_catenary[n,evev]==1; init=0)
                        for hh in Hours; init=0)
                    # Usually losses are covered 100% by RES to avoid USC
                    + sum(EV_CHARGE[n,ev,hh] - sum(EV_DISCHARGE[n,evev,hh] for evev in [ev] if ev_discharge[n,evev]==1; init=0) - EV_GED[n,ev,hh]/ev_efficiency_charge[n,ev] 
                    for hh in Hours; init=0)
                for ev in evTech if cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0; init=0)
            )
        end
    end

    # Add Module specific constraints

    # Both smart and exog
    @constraint(m, ev_chargelevel[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0],
        EV_L[n,ev,h] 
        == 
        + EV_L[n,ev,preceding_period(Hours,h)]
        + EV_CHARGE[n,ev,h] * ev_efficiency_charge[n,ev]
        + sum(EV_SWAP_TIME[n,evev,swap,h] for evev in [ev] for swap in swapTech if cond_swap(swap,n,dtr) && ev_swap[n,evev]==1; init=0)
        + EV_INF[n,ev,h]
        - sum(EV_DISCHARGE[n,evev,h]/ ev_efficiency_discharge[n,evev] for evev in [ev] if ev_discharge[n,evev]==1; init=0)
        - EV_GED[n,ev,h]
    );

    # Both smart and exog
    @constraint(m, ev_chargelevel_max[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_max_soc[n,ev] <= 1.0],
        EV_L[n,ev,h]
        <= 
        ev_capacity[n,ev] * ev_share[n,ev] * ev_quant[n] * ev_max_soc[n,ev]
    );

    # Both smart and exog
    @constraint(m, ev_chargelevel_min[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_min_soc[n,ev] > 0.0],
        EV_L[n,ev,h]
        >=
        ev_capacity[n,ev] * ev_share[n,ev] * ev_quant[n] * ev_min_soc[n,ev]
    );

    # Both smart and exog
    @constraint(m, ev_mobility_energy_demand[n=Nodes,ev=evTech,h=Hours;cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0],
        EV_ED[n,ev,h]
        == 
        + sum(EV_DIRECT[n,evev,h] for evev in [ev] if ev_catenary[n,evev]==1; init=0)
        + EV_GED[n,ev,h]
    );

    # Smart only (not exog)
    @constraint(m, ev_maxin_smart[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==0],
        EV_CHARGE[n,ev,h] 
        + sum(EV_DIRECT[n,evev,h] for evev in [ev] if ev_catenary[n,evev]==1; init=0)
        <=
        ev_p[n,ev,h] * ev_share[n,ev] * ev_quant[n]
    );

    # Smart only (not exog)
    @constraint(m, ev_maxout_smart[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==0],
        sum(EV_DISCHARGE[n,evev,h] for evev in [ev] if ev_discharge[n,evev]==1; init=0)
        <=
        ev_p[n,ev,h] * ev_bi[n,ev,h] * ev_share[n,ev] * ev_quant[n]
    );

    # Smart only (not exog)
    @constraint(m, ev_ed_smart[n=Nodes,ev=evTech, h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==0],
        EV_ED[n,ev,h]
        ==
        ev_ed[n,ev,h] * ev_share[n,ev] * ev_quant[n]
    );

    # exog exclusive 
    @constraint(m, ev_exog_ed_disabled_no_swap[n=Nodes,ev=evTech; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==1 && ev_swap[n,ev]==0],
        sum(EV_ED[n,ev,h] for h in Hours) 
        == 
        sum(ev_ged_exog[n,ev,h]  * ev_share[n,ev] * ev_quant[n] * ev_efficiency_charge[n,ev] for h in Hours)
    );

    # exog exclusive 
    @constraint(m, ev_exog_ed_disabled_swap[n=Nodes,ev=evTech; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==1 && ev_swap[n,ev]==1],
        sum(EV_ED[n,ev,h] for h in Hours) 
        == 
        sum(ev_ged_exog[n,ev,h]  * ev_share[n,ev] * ev_quant[n] for h in Hours)
    );



    # exog exclusive if not swap (represents energy taken from the grid - losses in exog)
    @constraint(m, ev_fixedin_no_swap[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==1 && ev_swap[n,ev]==0],
        EV_CHARGE[n,ev,h] 
        ==
        ev_ged_exog[n,ev,h] * ev_share[n,ev] * ev_quant[n]
    );

    # exog exclusive if swap (represents energy taken from the swap station - no losses in exog)
    @constraint(m, ev_fixedin_swap[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==1 && ev_swap[n,ev]==1],
        sum(EV_SWAP_TIME[n,ev,swap,h] for swap in swapTech if cond_swap(swap,n,dtr); init=0)
        ==
        ev_ged_exog[n,ev,h] * ev_share[n,ev] * ev_quant[n]
    );

    # exog exclusive if swap 
    @constraint(m, ev_charge_disabled_exog_swap[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==1 && ev_swap[n,ev]==1],
        EV_CHARGE[n,ev,h] == 0
    );

    # exog exclusive
    @constraint(m, ev_direct_disabled[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==1],
        sum(EV_DIRECT[n,evev,h] for evev in [ev] if ev_catenary[n,evev]==1; init=0) == 0
    );

    # exog exclusive
    @constraint(m, ev_discharge_disabled[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==1],
        sum(EV_DISCHARGE[n,evev,h] for evev in [ev] if ev_discharge[n,evev]==1; init=0) == 0
    );

    # swap station
    @constraint(m, SwapStorageBalance[n=Nodes, swap=swapTech, h=Hours; cond_swap(swap,n, dtr) && any(ev_swap[n,ev]==1 for ev in evTech if cond_ev(ev,n,dtr))],
        EV_SWAP_L[n, swap, h]
        ==
        swap_efficiency_static[n,swap]*EV_SWAP_L[n,swap,preceding_period(Hours,h)]
        + EV_SWAP_IN[n,swap,h]*swap_efficiency_charge[n,swap]
        - sum(EV_SWAP_TIME[n,ev,swap,h] for ev in evTech if cond_ev(ev,n,dtr) && ev_swap[n,ev]==1; init=0)
    ); # Swap station storage equation

    @constraint(m, SwapTimeEnergyMax[n=Nodes, ev=evTech, swap=swapTech, h=Hours; cond_ev(ev,n,dtr) && cond_swap(swap,n, dtr) && ev_swap[n,ev]==1],
        EV_SWAP_TIME[n,ev,swap,h] <= ev_sw[n,ev,swap,h] * ev_capacity[n,ev] * ev_share[n,ev] * ev_quant[n]
    );

    @constraint(m, SwapMaxWithdrawStorage[n=Nodes, swap=swapTech, h=Hours; cond_swap(swap,n, dtr) && any(ev_swap[n,ev]==1 for ev in evTech if cond_ev(ev,n,dtr))],
        EV_SWAP_IN[n,swap,h] <= EV_SWAP_N_P[n,swap]
    ); # Maximum storage charge

    @constraint(m, SwapMaxLevelStorage[n=Nodes,swap=swapTech, h=Hours; cond_swap(swap,n, dtr) && any(ev_swap[n,ev]==1 for ev in evTech if cond_ev(ev,n,dtr))],
        EV_SWAP_L[n,swap,h] <= EV_SWAP_N_E[n,swap]
    ); # Maximum storage level

    # new constraint to ensure that storage level is ready to be charged
    @constraint(m, SwapLevelStorageBeforeSwap[n=Nodes,swap=swapTech, h=Hours; cond_swap(swap,n, dtr) && any(ev_swap[n,ev]==1 for ev in evTech if cond_ev(ev,n,dtr))],
        EV_SWAP_L[n,swap,preceding_period(Hours,h)] >= sum(EV_SWAP_TIME[n,ev,swap,h] for ev in evTech if cond_ev(ev,n,dtr) && ev_swap[n,ev]==1; init=0)
    );

    @constraint(m, EV_INF_Disabled[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && infeas_ev_cost==-1],
        EV_INF[n,ev,h] == 0
        );

    @constraint(m, ev_discharge_generation_capacity_bound[n=Nodes, h=Hours; cond_ext(:ev,n,dtr) && ev_discharge_cap_share[n] > 0],
        sum(EV_DISCHARGE[n,ev,h] for ev in evTech if cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==0 && ev_discharge[n,ev]==1; init=0)
        <= ev_discharge_cap_share[n] * sum(m[:N_TECH][n,t] for t in Technologies if cond_tech(t,n, dtr) && MaxInstallable[n,t] > 0; init=0)
    );

    @constraint(m, ev_charge_generation_capacity_bound[n=Nodes, h=Hours; cond_ext(:ev,n,dtr) && ev_charge_cap_share[n] > 0],
        sum(EV_CHARGE[n,ev,h] for ev in evTech if cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_exog[n,ev]==0; init=0)
        <= ev_discharge_cap_share[n] * sum(m[:N_TECH][n,t] for t in Technologies if cond_tech(t,n, dtr) && MaxInstallable[n,t] > 0; init=0)
    );

    @constraint(m, ev_charge_disabled[n=Nodes,ev=evTech,h=Hours; cond_ext(:ev,n,dtr) && cond_ev(ev,n,dtr) && ev_share[n,ev] > 0 && ev_charges[n,ev]==0],
        EV_CHARGE[n,ev,h] == 0
    );

end


