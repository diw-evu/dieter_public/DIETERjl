## Two separate functions for decentral (simple) heating and district heating
## District heating is only activable if the heat module is active


function heat_module(m::JuMP.Model,dtr::DieterModel,obj_expr, ebal, min_res_expr, corr_factor, scale_dict)
    # update maps
    update_map(dtr;feature=:heat, category=:base, maps=get_feature_map(dtr; symbol=:heat_enabled, feature=:heat))

    # Sets
    Hours - dtr.sets[:h]
    Nodes = dtr.sets[:n]
    HeatSink = dtr.sets[:heat_sink] # e.g. floor, water
    Building = dtr.sets[:building] # e.g. SFH, MFH, COM
    HeatTech = dtr.sets[:heat_tech] # e.g. ground-sourced HP, air-sourced HP

    # Scaling factors
    sf_ener = !isnothing(scale_dict) ? scale_dict[:energy] : 1.0
    sf_curr = !isnothing(scale_dict) ? scale_dict[:currency] : 1.0


    # Heat demand calculations
    heat_dc_demand                 = dtr.parameters[:dc_heat_demand]*sf_ener # DenseAxisArray with heat demand by node, building, sink, and hour
    # Coefficients of performance
    heat_dc_cop                    = dtr.parameters[:dc_cop] # DenseAxisArray with COP by node, technology and hour

    # Share of technology of total heat demand
    heat_share                     = dtr.parameters[:heat_share] # DenseAxisArray with share of technology of total heat demand by node, building, sink, and hour
    
    # Technology parameters
    heat_storage_efficiency        = dtr.parameters[:heat_storage_efficiency]
    heat_storage_static_efficiency = dtr.parameters[:heat_storage_static_efficiency]
    heat_storage_duration          = dtr.parameters[:heat_storage_duration]



    # Variables for module
    @variables(m,begin
        # Decentral heating
        HEAT_ELEC_IN[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours;      cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0 ]>=0
        HEAT_STO_OUT[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours;     cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0]>=0
        HEAT_STO_IN[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours;      cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0]>=0
        HEAT_STO_L[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours;       cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0]>=0

        HEAT_N_HP[n=Nodes,b=Building,s=HeatSink,t=HeatTech;      cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0]>=0
        HEAT_N_STO_E[n=Nodes,b=Building,s=HeatSink,t=HeatTech;      cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0]>=0
    end);

    # Decentral heating
    @constraint(m,HeatEbal[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours; cond_ext(:heat,n,dtr)&& heat_share(n,b,s,t)>=0],
        HEAT_STO_OUT[n,b,s,t,h] - heat_share[n,b,s,t]*heat_dc_demand[n,b,s,h] == 0
    );

    @constraint(m,HeatStorageBalance[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours; cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0],
        HEAT_STO_L[n,b,s,t,h] == 
        heat_storage_static_efficiency*HEAT_STO_L[n,b,s,t,preceding_period(Hours,h)] + 
        heat_storage_efficiency*HEAT_STO_IN[n,b,s,t,h] - 
        HEAT_STO_OUT[n,b,s,t,h]/heat_storage_efficiency
    );

    @constraint(m,HeatHPIn[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours; cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0],
        heat_dc_cop[n,s,t,h]*HEAT_ELEC_IN[n,b,s,t,h] == HEAT_STO_IN[n,b,s,t,h]
    );

    @constraint(m,HeatStorageMax[n=Nodes,b=Building,s=HeatSink,t=HeatTech; cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0],
        HEAT_STO_L[n,b,s,t,h] <= HEAT_N_STO_E[n,b,s,t]
    );

    @constraint(m,HeatHPMaxIn[n=Nodes,b=Building,s=HeatSink,t=HeatTech; cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0],
        HEAT_STO_IN[n,b,s,t,h] <= HEAT_N_HP[n,b,s,t,h]
    );

    @constraint(m,HeatStoMaxOut[n=Nodes,b=Building,s=HeatSink,t=HeatTech; cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0],
        HEAT_STO_OUT[n,b,s,t,h] <= HEAT_N_HP[n,b,s,t,h]
    );

    @constraint(m,HeatStorageDuration[n=Nodes,b=Building,s=HeatSink,t=HeatTech; cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0],
        HEAT_N_STO_E[n,b,s,t] <= heat_storage_duration[n,b,s,t]*HEAT_N_HP[n,b,s,t]
    );

    @constraint(m,HeatInstalledOut[n=Nodes,b=Building,s=HeatSink,t=HeatTech; cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0],
        HEAT_N_STO_IN[n,b,s,t] == heat_share[n,b,s,t]*maximum(heat_dc_demand[n,b,s,:])/heat_storage_efficiency
    );

    # Changes in the electricity energy balance
    for n in Nodes
        if length(Hours)>0
            for h in Hours
                add_to_expression!(m,ebal[n,h],
                    # Demand side
                    - sum(HEAT_ELEC_IN[n,b,s,t,h] for b in Building for s in HeatSink for t in HeatTech if cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0; init = 0)
            );
            end
        end
    end

    # Changes in the minimum renewables constraint - all additional electricity demand from heat pumps need to be covered by renewables
    for n in Nodes
        if length(Hours)>0 
            add_to_expression!(m,min_res_expr[n],
                # Demand side
                + sum(HEAT_ELEC_IN[n,b,s,t,h] for b in Building for s in HeatSink for t in HeatTech for h in Hours if cond_ext(:heat,n,dtr) && heat_share(n,b,s,t)>=0; init = 0)
            );
        end
    end

    # Objective function
    # In the current version there are no changes to the objective function

end


function district_heat_module(m::JuMP.Model,dtr::DieterModel,obj_expr, ebal, min_res_expr, corr_factor)
    # District heating #########################################################

    # Includes resistive heating at a COP of 1
    @constraint(m,DHGen[n=Nodes,h=Hours],
        sum(dh_cop[n,t,h]*DH_ELEC_IN[n,t,h] for t in DHTech) + DH_OTHER[n,h] == DH_STO_IN[n,h]
    );

    # Define other heat inp ut from waste heat and or biomass
    @constraint(m,DHOther[n=Nodes,h=Hours],
        DH_OTHER[n,h] <=  waste_share * dh_total_demand[n] + bio_chp_share*chp_share*m[:G][n,"bio",h]
    ); 

    # Maximum heat generation 
    @constraint(m,DHGenMax[n=Nodes,t=DHTech,h=Hours],
        dh_cop[n,t,h]*DH_ELEC_IN[n,t,h] <= DH_N_HP[n,t,h]
    );

    @constraint(m,DHStorageBalance[n,h],
        DH_STO_L[n,h] == 
        (1-dh_standing_losses)*DH_STO_L[n,preceding_period(Hours,h)] 
        + dh_sto_eff*DH_STO_IN[n,h] 
        - DH_STO_OUT[n,h]/dh_sto_eff
    );

    # Serve heat demand
    @constraint(m,DHHeatDemand[n=Nodes,b=Building,s=HeatSink,t=DHTech,h=Hours],
        DH_STO_OUT[n,h] - dh_heat_demand[n,h] == 0
    );

    # Maximum storage level
    @constraint(m,DHStorageMax[n=Nodes,h=Hours],
        DH_STO_L[n,h] <= DH_N_STO_E[n]
    );

    # Heat energy balances
    @constraint(m,HeatEbal[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours],
        HEAT_STO_OUT[n,b,s,t,h] - heat_share[n,b,s,t]*dc_head_demand[n,b,s,h] == 0
    );

    # Electricity demand
    @constraint(m,HeatElecDemand[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours],
        dc_cop[n,s,t,h]*HEAT_ELEC_IN[n,b,s,t,h] - HEAT_STO_IN[n,b,s,t,h] == 0
    );

    # Storage plant equation
    @constraint(m,HeatStoPlant[n=Nodes,b=Building,s=HeatSink,t=HeatTech,h=Hours],
        HEAT_STO_L[n,b,s,t,h] == 
        (1-dc_standing_losses[n,b,s,t])*HEAT_STO_L[n,b,s,t,preceding_period(Hours,h)] + 
        heat_sto_eff[n,b,s,t]*HEAT_STO_IN[n,b,s,t,h] - 
        HEAT_STO_OUT[n,b,s,t,h]/heat_sto_eff[n,b,s,t]
    );

    # Installed heating power
    @constraint(m,HeatInstalledOut[n=Nodes,b=Building,s=HeatSink,t=HeatTech],
        HEAT_N_STO_OUT[n,b,s,t] == heat_share[n,b,s,t]*maximum(dc_heat_demand[n,b,s,:])/heat_sto_eff[n,b,s,t]
    );

    @constraint(m,HeatInstalledIn[n=Nodes,b=Building,s=HeatSink,t=HeatTech],
        HEAT_N_STO_OUT[n,b,s,t] == HEAT_N_STO_IN[n,b,s,t]
    );



end
