
function cond_heat(heat_tech::AbstractString, building::AbstractString, node::AbstractString, dtr::DieterModel)
    if heat_tech in dtr.maps[:heat][:base][node][building]
        return true
    else
        return false
    end
end


function heat_module_old(m::JuMP.AbstractModel,dtr::DieterModel, obj_expr, ebal, min_res_expr, corr_factor)
    """
    Add Heat Module
    Description: Defines sets, variables, constraints and modifies existing constraints, e.g. the energy balance,
    and the objective function. This function can only be called from inside the define_model function
    """

    update_map(dtr;feature=:heat, category=:base, maps=get_feature_map(dtr; symbol=:heat_enabled, feature=:heat))
    # fill map here (see other at the begining of ev_module or h2_module)
    # any calc here


    # Define module-specific sets
    Nodes = dtr.sets[:n]
    Hours = dtr.sets[:h]
    BuildType = dtr.sets[:buildings]
    HeatTech = dtr.sets[:heat_tech]
    Heat2Sto = dtr.sets[:heat_2sto]
    HeatPump = dtr.sets[:heat_pump]
    HeatElec = dtr.sets[:heat_elec]
    HeatFossil = dtr.sets[:heat_fossil]
    # HeatElectric = 

    # ...


    # Extract module-specific parameters
    infeas_h_cost = dtr.settings[:infeas_h_cost] # from scalar.csv
    HeatShares = dtr.parameters[:heat_share]
    FuelHeatPenality = dtr.parameters[:fuel_heat_penalty]
    StaticEfficiency = dtr.parameters[:static_efficiency]
    DynamicEfficiency = 
    temp_adjust = DenseAxisArray([-10,5],["hp_gs","hp_as"])
    temp_sink = 
    SecMarginHeatOut =
    ep_ratio = 
    WarmWaterLoad = 
    HeatLoad = 
    n_sets_dhw_p_in = 
    n_sets_dhw_e = 


    # Define module-specific variables
    @variable(m,H_DIR[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0) # Direct heating
    @variable(m,H_SETS_LEV[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_SETS_IN[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_SETS_OUT[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_HP_IN[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_STO_LEV[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_STO_IN_HP[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_STO_IN_ELECTRIC[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_ELECTRIC_IN[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_STO_IN_FOSSIL[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_STO_OUT[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_STO_LEV_INI[n=Nodes,bu=BuildType,ch=HeatTech; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,N_HEAT_P_OUT[n=Nodes,bu=BuildType,ch=HeatTech; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,N_HEAT_P_IN[n=Nodes,bu=BuildType,ch=HeatTech; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,N_HEAT_E[n=Nodes,bu=BuildType,ch=HeatTech; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_DHW_DIR[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_DHW_STO_OUT[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_DHW_AUX_ELEC_IN[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_DHW_AUX_LEV[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_DHW_AUX_OUT[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >= 0)
    @variable(m,H_INF[n=Nodes,bu=BuildType,h=Hours; cond_ext(:heat,n,dtr)] >=0)
    @variable(m,H_DHW_INF[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours; cond_ext(:heat,n,dtr)] >=0)

    # Modify objective function
    add_to_expression!(obj_expr,
    sum(FuelHeatPenality[n,hfo]*H_STO_IN_FOSSIL[n,bu,hfo,h] for h in Hours, n in Nodes, bu in BuildType, hfo in HeatFossil if cond_ext(:heat,n,dtr))
    + sum(infeas_h_cost*H_INF[n,bu,h] for h in Hours, n in Nodes, bu in BuildType if cond_ext(:heat,n,dtr))
    + sum(infeas_h_cost*H_DHW_INF[n,bu,ch,h] for h in Hours, n in Nodes, bu in BuildType, ch in HeatTech if cond_ext(:heat,n,dtr))
    );

    # Modify energy balance
    for h in Hours, n in Nodes
        add_to_expression!(ebal[n,h],
        # Demand side
        - sum(H_DIR[n,bu,ch,h]+H_DHW_DIR[n,bu,ch,h] for ch in HeatTech, bu in BuildType if cond_ext(:heat,n,dtr))
        - sum(H_SETS_IN[n,bu,ch,h]+H_DHW_AUX_ELEC_IN[n,bu,ch,h] for ch in HeatTech, bu in BuildType if cond_ext(:heat,n,dtr))
        - sum(H_HP_IN[n,bu,hp,h] for hp in HeatPump, bu in BuildType if cond_ext(:heat,n,dtr))
        - sum(H_ELECTRIC_IN[n,bu,hel,h] for hel in HeatElectric, bu in BuildType if cond_ext(:heat,n,dtr))
        );
    end
    # Add Module specific constraints
        # Heat energy balance
    @constraint(m,HeatEnergyBalance[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours2; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_DIR[n,bu,ch,h]
        +H_SETS_OUT[n,bu,ch,h]
        +H_STO_OUT[n,bu,ch,h]
        +(1-StaticEfficiency[n,ch])*H_SETS_LEV[n,bu,ch,(h-1)]
        ==
        HeatLoad[n,bu,ch,h] - H_INF[n,bu,ch,h] # Heat energy balance from period 2 for every node n, house type bu equipped with heating system ch at period h
    );

    @constraint(m,HeatEnergyBalanceFirstPeriod[n=Nodes,bu=BuildType,ch=HeatTech; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_DIR[n,bu,ch,1]
        +H_SETS_OUT[n,bu,ch,1]
        +H_STO_OUT[n,bu,ch,1]
        +(1-StaticEfficiency[n,ch])*H_SETS_LEV[n,bu,ch,Hours[end]]
        ==
        HeatLoad[n,bu,ch,1] - H_INF[n,bu,ch,1] # Heat energy balance for period 1
    );

    # Warm water energy balance
    @constraint(m,WarmWaterEnergyBalance[n=Nodes,bu=BuildType,ch=HeatTech,h=Hours2; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_DHW_DIR[n,bu,ch,h]
        +H_DHW_AUX_OUT[n,bu,ch,h]
        +H_DHW_STO_OUT[n,bu,ch,h]
        +(1-StaticEfficiency[n,ch])*H_DHW_AUX_LEV[n,bu,ch,(h-1)]
        ==
        WarmWaterLoad[n,bu,ch,h] - H_DHW_INF[n,bu,ch,h] # Warm water energy balance from period 2
    );

    @constraint(m,WarmWaterEnergyBalanceFirstPeriod[n=Nodes,bu=BuildType,ch=HeatTech; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_DHW_DIR[n,bu,ch,1]
        +H_DHW_AUX_OUT[n,bu,ch,1]
        +H_DHW_STO_OUT[n,bu,ch,1]
        +(1-StaticEfficiency[n,ch])*H_DHW_AUX_LEV[n,bu,ch,Hours(end)]
        ==
        WarmWaterLoad[n,bu,ch,1] - H_DHW_INF[n,bu,ch,1] # Warm water energy balance for period 1
    );

    #--------------------------------------------------------------------------
    # SETS
    #--------------------------------------------------------------------------
    @constraint(m,SETSLevel[n=Nodes,bu=BuildType,h=Hours2; cond_ext(:heat,n,dtr) && cond_heat("setsh",bu,n,dtr)],
        H_SETS_LEV[n,bu,"setsh",h]
        ==
        StaticEfficiency[n,"setsh"]*H_SETS_LEV[n,bu,"setsh",(h-1)]+DynamicEfficiency["setsh"]*H_SETS_IN[n,bu,"setsh",h]-H_SETS_OUT[n,bu,"setsh",h]
    ); # SETS Storage dynamics

    @constraint(m,SETSLevelFirstPeriod[n=Nodes,bu=BuildType; cond_ext(:heat,n,dtr) && cond_heat("setsh",bu,n,dtr)],
        H_SETS_LEV[n,bu,"setsh",1]
        ==
        StaticEfficiency[n,"setsh"]*H_SETS_LEV[n,bu,"setsh",Hours[end]]+DynamicEfficiency["setsh"]*H_SETS_IN[n,bu,"setsh",1]-H_SETS_OUT[n,bu,"setsh",1]
    ); # SETS Storage dynamics first period

    @constraint(m,SETSMaxIn[n=Nodes,bu=BuildType,h=Hours; cond_ext(:heat,n,dtr) && cond_heat("setsh",bu,n,dtr)],
        H_SETS_IN[n,bu,"setsh",h]
        <= 
        n_sets_p_in[n,bu]
    ); # SETS max in

    @constraint(m,SETSMaxOut[n=Nodes,bu=BuildType,h=Hours; cond_ext(:heat,n,dtr) && cond_heat("setsh",bu,n,dtr)],
        H_SETS_OUT[n,bu,"setsh",h]
        <=
        n_sets_p_out[n,bu]
    ); # SETS max

    #@constraint(m,SETSMinIn[n=Nodes,bu=BuildType,h=Hours; cond_ext(:heat,n)],
    #    - H_SETS_IN[n,bu,"setsh",h] + n_sets_p_
    #); """ Only relevant in conjunction with reserves """

    @constraint(m,SETSMaxLevel[n=Nodes,bu=BuildType,h=Hours; cond_ext(:heat,n,dtr) && cond_heat("setsh",bu,n,dtr)],
        H_SETS_LEV[n,bu,"setsh",h]
        <=
        n_sets_e[n,bu]
    );

    @constraint(m,SETSAuxDhwLevel[n=Nodes,bu=BuildType,h=Hours2; cond_ext(:heat,n,dtr) && cond_heat("setsh",bu,n,dtr)],
        H_DHW_AUX_LEV[n,bu,"setsh",h]
        ==
        StaticEfficiency[n,"setsh"]*H_DHW_AUX_LEV[n,bu,"setsh",(h-1)]+DynamicEfficiency("setsh")*H_DHW_AUX_ELEC_IN[n,bu,"setsh",h]-H_DHW_AUX_OUT[n,bu,"setsh",h]
    ); # SETS Storage dynamics warm water

    @constraint(m,SETSAuxDhwLevelFirstPeriod[n=Nodes,bu=BuildType; cond_ext(:heat,n,dtr) && cond_heat("setsh",bu,n,dtr)],
        H_DHW_AUX_LEV[n,bu,"setsh",1]
        ==
        StaticEfficiency[n,"setsh"]*H_DHW_AUX_LEV[n,bu,"setsh",Hours[end]]+DynamicEfficiency("setsh")*H_DHW_AUX_ELEC_IN[n,bu,"setsh",1]-H_DHW_AUX_OUT[n,bu,"setsh",1]
    ); # SETS Storage dynamics first period warm water

    @constraint(m,SETSAuxDhwMaxIn[n=Nodes,bu=BuildType,h=Hours; cond_ext(:heat,n,dtr) && cond_heat("setsh",bu,n,dtr)],
        H_DHW_AUX_ELEC_IN[n,bu,"seth",h] 
        <=
        n_sets_dhw_p_in[n,bu] # Sets max in warm water
    );

    @constraint(m,SETSAuxDhwMaxLevel[n=Nodes,bu=BuildType,h=Hours; cond_ext(:heat,n,dtr) && cond_heat("setsh",bu,n,dtr)],
        H_AUX_DHW_LEV[n,bu,"setsh",h]
        <=
        n_sets_dhw_e[n,bu]
    );

    #--------------------------------------------------------------------------
    # Heat pumps
    #--------------------------------------------------------------------------

    @constraint(m, HPIn[n=Nodes,bu=BuildType,ch=HeatPump,h=Hours; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_STO_IN_HP[n,bu,ch,h]
        ==
        H_HP_IN[n,bu,ch,h]*DynamicEfficiency[ch]*CoP[n,ch,h] # Heat pump contribution to heat storage level
    );

    @constraint(m, HPMaxIn[n=Nodes,bu=BuildType,ch=HeatPump,h=Hours; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_HP_IN[n,bu,ch,h]
        <=
        N_HEAT_P_IN[n,cu,ch] # maximum inflow heat pumps
    );

    # Skipped minimum inflow since only relevant in combination with reserves

    #--------------------------------------------------------------------------
    # Hybrid electric heating
    #--------------------------------------------------------------------------

    @constraint(m, ElecStoIn[n=Nodes,bu=BuildType,ch=HeatElec,h=Hours; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_STO_IN_ELECTRIC[n,bu,ch,h]
        ==
        H_ELECTRIC_IN[n,bu,ch,h]
    ); # Electric heating contribution to heat storage level

    @constraint(m, ElecStoIn[n=Nodes,bu=BuildType,ch=HeatElec,h=Hours; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_IN_ELECTRIC[n,bu,ch,h]
        <=
        N_HEAT_P_IN[n,bu,ch,h]
    ); # Electric heat, max in

    # Skipped minimum since only relevant in combination with reserves

    #--------------------------------------------------------------------------
    # Heat storage
    #--------------------------------------------------------------------------

    @constraint(m, HeatStoLevel[n=Nodes,bu=BuildType,ch=Heat2Sto,h=Hours2; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_STO_LEV[n,bu,ch,h]
        ==
        StaticEfficiency[n,ch]*H_STO_LEV[n,bu,ch,(h-1)]
        + H_STO_IN_HP[n,bu,ch,h]
        + H_STO_IN_ELECTRIC[n,bu,ch,h]
        + H_STO_IN_FOSSIL[n,bu,ch,h]
        - H_STO_OUT[n,bu,ch,h]
        - H_DHW_STO_OUT[n,bu,ch,h]
    ); # Heat storage plant equation

    @constraint(m, HeatStoLevel[n=Nodes,bu=BuildType,ch=Heat2Sto; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_STO_LEV[n,bu,ch,1]
        ==
        StaticEfficiency[n,ch]*H_STO_LEV[n,bu,ch,Hours[end]]
        + H_STO_IN_HP[n,bu,ch,1]
        + H_STO_IN_ELECTRIC[n,bu,ch,1]
        + H_STO_IN_FOSSIL[n,bu,ch,1]
        - H_STO_OUT[n,bu,ch,1]
        - H_DHW_STO_OUT[n,bu,ch,1]
    ); # Heat storage plant equation - first period

    @constraint(m, MaxHeatStoLevel[n=Nodes,bu=BuildType,ch=Heat2Sto,h=Hours; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        H_STO_LEV[n,bu,ch,h] <= N_HEAT_E[n,bu,ch]
    ); # Maximum heat energy storage

    #--------------------------------------------------------------------------
    # Power and energy capacities
    #--------------------------------------------------------------------------


    @constraint(m, HeatCapaOut[n=Nodes,bu=BuildType, ch=HeatTech; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        N_HEAT_P_OUT[n,bu,ch]
        ==
        SecMarginHeatOut * MaxDemand[n,bu,ch]
    ); # Define output capacity by maximum demand + security MA

    @constraint(m, HeatCapaIn[n=Nodes,bu=BuildType, ch=HeatTech; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        N_HEAT_P_IN[n,bu,ch]
        ==
        N_HEAT_P_OUT[n,bu,ch]
    );

    @constraint(m, HeatCapaEnergy[n=Nodes,bu=BuildType, ch=HeatTech; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        N_HEAT_E[n,bu,ch]
        ==
        ep_ratio[n,bu,ch] * N_HEAT_P_OUT[n,bu,ch]
    );

    @constraint(m, HeatCapaInHeatPump[n=Nodes,bu=BuildType,ch=["hp_as","hp_gs"]; cond_ext(:heat,n,dtr) && cond_heat(ch,bu,n,dtr)],
        N_HEAT_P_IN[n,bu,ch] 
        ==
        N_HEAT_P_OUT[n,bu,ch]/(DynamicEfficiency[ch]*(temp_sink[ch]+273.15)/(temp_sink[ch]-temp_adjust[ch]))
    );

end